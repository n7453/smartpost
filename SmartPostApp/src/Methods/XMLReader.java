//Tekijä: Vilma Mäkitalo
//Päivämäärä: 16.06.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)


package Methods;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLReader {

	private static XMLReader XMLR = null;
	
	private XMLReader(){
	}
	
	public static XMLReader getInstance() {
		
		if(XMLR == null) {
			XMLR = new XMLReader();
		}
		return XMLR;
	}
	
	public void readXML(String url, Connection con) {
		
		String content = readURL(url);
		
		makeDocument(content, con);
		
		
	}
	
	private static String readURL(String u) {
		
		
		String line;
		String content = "";
		BufferedReader br = null;
		
		try {
			URL url = new URL(u);
			
			br = new BufferedReader(new InputStreamReader(url.openStream()));
			
			while ((line = br.readLine()) != null) {
				content += line + "\n";
			};
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}
	
	private static void makeDocument(String c, Connection con) {
		
		String content = c;
		
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
			Document doc = dBuilder.parse(new InputSource(new StringReader(content)));
			
			doc.getDocumentElement().normalize();

			parseCurrentData(doc, con);
			
			} catch (SAXException | IOException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			}
		}
	
	private static void parseCurrentData(Document doc, Connection con) {
		
		NodeList nodes = doc.getElementsByTagName("place");
		String city, address, availability, postoffice;
		double lat, lng;
		
		for(int i = 0; i < nodes.getLength(); i++) {
			int k = 0;
			Node node = nodes.item(i);
			Element e = (Element) node;
			city = getValue("city",e).toUpperCase();
			address = getValue("address",e);
			availability = getValue("availability",e);
			postoffice = getValue("postoffice",e);
			lat = Double.parseDouble(getValue("lat",e));
			lng = Double.parseDouble(getValue("lng",e));	
			try {
				ResultSet rs = con.createStatement().executeQuery("SELECT \"Postoffice\" FROM \"SmartPost\";");
				while(rs.next()) {
					if(!postoffice.contains(rs.getString("Postoffice"))) {
					}
					else {
						k = 1;
						break;
					}
				}	
				if(k == 0) {
					con.createStatement().execute("INSERT INTO \"SmartPost\" VALUES('"+postoffice+"','"+availability+"','"+address+"',"+lat+","+lng+",'"+city+"');");
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private static String getValue(String tag, Element e) {
		return e.getElementsByTagName(tag).item(0).getTextContent();
	}
}
