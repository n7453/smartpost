//Tekijä: Vilma Mäkitalo
//Päivämäärä: 10.07.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)

package Methods;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Safety {

	static private Safety safe = null;
	private String activeUser;
	private int activeSecurityLevel;
	
	private Safety() {
		
	}
	
	static public Safety getInstance(){
		
		if(safe == null) {
			safe = new Safety();
		}
		return safe;
	}
	
	//Set current username
	public void setUser(String name) {
		activeUser = name;
	}
	
	//Get current username
	public String getUserName() {
		return activeUser;
	}
	
	//Get users name
	public String getUserRealName(String user, Connection con) {
		
		String name = null;
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"RealName\" FROM \"User\" WHERE \"UserName\" = '" + user + "';");
			while(rs.next()) {
				name = rs.getString("RealName");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(name == null) {
			return user;
		}
		return name;
	}

	//Get current securityLevel
	public int getSecurityLevel() {
		return activeSecurityLevel;
	}
	
	//Popup for loging in or signing in
	public void logInPopup() {
		
		try {
			
			Stage view = new Stage();		//Popup window for loging or signing in
			Parent page = FXMLLoader.load(getClass().getResource("/FXML/LogIn.fxml"));
			Scene scene = new Scene(page);
			
			view.setScene(scene);
			scene.getStylesheets().add(getClass().getResource("/Css/LogIn.css").toExternalForm());
			view.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Check username and password, and log in if ok
	public int logIn(String user, String password, Connection con) {
		
		String truePassword = null;
		int i = 0;
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Password\",\"SecurityLevel\" FROM \"User\" WHERE \"UserName\" = '" + user + "';");
			while(rs.next()) {
				activeUser = user;
				truePassword = rs.getString("Password");
				activeSecurityLevel = rs.getInt("SecurityLevel");
			}
			if(truePassword != null) {
				if(truePassword.equals(password)) {
					i = 1;
				}
				else {
					activeUser = null;
				}
			}
			else {
				activeUser = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return i;
	}
	
	//Sign in as host
	public int signInHost(String name, String userName, String password, String dPassword, String safeCode, Connection con) {
		
		int i = 0;
		
		if(password.equals(dPassword)) {
			if(userName.length() <= 20 && password.length() <= 20){
				ResultSet rs;
				try {
					rs = con.createStatement().executeQuery("SELECT * FROM \"Code\" WHERE \"SafetyCode\" = '" + safeCode + "';");
					i = 2;
					while(rs.next()){
							con.createStatement().execute("INSERT INTO \"User\" VALUES('" + userName + "','" + name + "','" + password + "', 1);");
							con.createStatement().execute("DELETE FROM \"Code\" WHERE \"SafetyCode\" = '" + safeCode + "';");
							i = 4;
					}
				} catch (SQLException e) {
					i = 3;
					e.printStackTrace();
				}
			}
			else {
				i = 1;
			}
		}
		return i;
	}
	
	//Sign in as host
	public int signInUser(String name, String userName, String password, String dPassword, Connection con) {
		
		int i = 0;
		
		if(password.equals(dPassword)) {
			if(userName.length() <= 20 && password.length() <= 20){
				try {
					con.createStatement().execute("INSERT INTO \"User\" VALUES('" + userName + "','" + name + "','" + password + "', 2);");
					i = 3;
				} catch (SQLException e) {
					i = 2;
					e.printStackTrace();
				}
			}
			else {
				i = 1;
			}
		}
		return i;
	}
	
	//Removes old securitycode from database
	public void updateSafetyCode(Connection con) {
		
		Calendar c = Calendar.getInstance();
		c.getTime();
		c.add(Calendar.DATE, -30);
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"SafetyCode\",\"CreateDate\" FROM \"Code\";");
			while(rs.next()) {
				String code = rs.getString("SafetyCode");
				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString("CreateDate"));
				
				if(date.before(c.getTime())){
					con.createStatement().execute("DELETE FROM \"Code\" WHERE \"SafetyCode\" = '" + code + "';");
				}
			}
		} catch (SQLException | ParseException e) {
			e.printStackTrace();
		}
		
	}
	
	//Adds new safetycode to database
	public int addSafetyCode(String code, Connection con) {
		
		if(code.matches("^[a-zA-Z0-9]*$")) {
			String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
			try {
				con.createStatement().execute("INSERT INTO \"Code\" VALUES('" + code + "','" + date + "');");
			} catch (SQLException e) {
				e.printStackTrace();
				return 2;
			}
			return 0;
		}
		else {
			return 1;
		}
	}

	//Get registered users, no hosts
	public ArrayList<String> getUsers(Connection con) {
		
		ArrayList<String> users = new ArrayList<String>();
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"UserName\" FROM \"User\" WHERE NOT \"UserName\" = '" + activeUser + "' AND \"SecurityLevel\" = 2;");
			while(rs.next()) {
				users.add(rs.getString("UserName"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}

	//Removes account. User or host
	public int removeAccount(Connection con) {
		
		int i = 0;
		
		ResultSet rs;
		if(activeSecurityLevel == 1) {
			try {
				rs = con.createStatement().executeQuery("Select COUNT(*) FROM \"User\" WHERE \"SecurityLevel\" = 1;");
				while(rs.next()) {
					i = rs.getInt("COUNT(*)");
				}
				if(i <= 1) {
					return 1;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		String realName = getUserRealName(activeUser, con);
		
		try {
			con.createStatement().execute("UPDATE \"Package\" SET \"SendFrom\" = '" + realName + "' WHERE \"SendFrom\" = '" + activeUser + "';");
			con.createStatement().execute("UPDATE \"Package\" SET \"SendTo\" = '" + realName + "' WHERE \"SendTo\" = '" + activeUser + "';");
			con.createStatement().execute("DELETE FROM \"User\" WHERE \"UserName\" = '" + activeUser + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return 0;
	}	
}
