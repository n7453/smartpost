//Tekijä: Vilma Mäkitalo
//Päivämäärä: 4.7.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)

package Methods;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;

import Main.SmartPostManager;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.stage.Stage;
import javafx.util.Callback;


public class Log {

	static private Log log = null;
	static private SmartPostManager SPM;
	
	private Log(){
		
	}
	
	static public Log getInstance(){
		if(log == null) {
			log = new Log();
			SPM = SPM.getInstance();
		}
		return log;
	}
	
	//Popup for asking if reset warehouse or continue with old information
	public void askWarehouseReset() {
		
		try {
			
			Stage view = new Stage();		//Popup window for asking if reset
			Parent page = FXMLLoader.load(getClass().getResource("/FXML/ResetWarehouse.fxml"));
			Scene scene = new Scene(page);
			
			view.setScene(scene);
			scene.getStylesheets().add(getClass().getResource("/Css/ResetWarehouse.css").toExternalForm());
			view.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Get information from log
	public ObservableList<ObservableList<String>> getLogInformation(Connection con) {
		
		ObservableList<ObservableList<String>> info = FXCollections.observableArrayList();
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Log\".\"PackageID\",\"Route\".\"FromSmartPost\", \"Route\".\"ToSmartPost\",\"SentDate\", \"Fractured\",\"Successful\" FROM \"Log\" INNER JOIN \"Package\" ON (\"Package\".\"PackageID\" = \"Log\".\"PackageID\") INNER JOIN \"Route\" ON (\"Package\".\"RouteID\" = \"Route\".\"RouteID\");");
			while(rs.next()) {
				String s2 = "";
				
				String date2 = rs.getString("SentDate");
				String[] string2 = date2.split("-");
				s2 = string2[2] + "." + string2[1] + "." + string2[0];
				
				String[] str = new String[2];
				if(rs.getString("Fractured").equals("true")) {
					str[0] = "Rikki";
				}
				else {
					str[0] = "Ehjä";
				}
				if(rs.getString("Successful").equals("true")) {
					str[1] = "Onnistunut";
				}
				else {
					str[1] ="Väärä osoite";
				}
				
				info.add(FXCollections.observableArrayList(rs.getString("PackageID"),rs.getString("FromSmartPost"),rs.getString("ToSmartPost"), s2, str[0], str[1]));
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return info;
	}
	
	//Count how many packages are in warehouse
	public String getWarehouseSize(Connection con){
		
		String size = "";
		
		ResultSet rs;
		
		try {
			rs = con.createStatement().executeQuery("SELECT COUNT(*) FROM \"Warehouse\";");
			while(rs.next()) {
				size = rs.getString("COUNT(*)") + " kpl";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return size;
	}
		
	//Count how many packages have been sent
	public String getLogSize(Connection con){
		
		String size = "";
		
		ResultSet rs;
		
		try {
			rs = con.createStatement().executeQuery("SELECT COUNT(*) FROM \"Log\";");
			while(rs.next()) {
				size = rs.getString("COUNT(*)") + " kpl";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return size;
	}
	
	//Get information from package
	public ArrayList<String> getPackageInformation(String line, Connection con) {
		
		ArrayList<String> info = new ArrayList<String>();
		String item1 = null, item2 = null, item3 = null, item4 = null, amount1 = null, amount2 = null, amount3 = null, amount4 = null, str = "", tofrom = "";
		
		String[] s = line.split(",");
		int ID = Integer.parseInt(s[0].replaceAll("[\\D]", ""));
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Item1\",\"Item2\",\"Item3\",\"Item4\",\"Amount1\",\"Amount2\",\"Amount3\",\"Amount4\", \"Package\".\"SendFrom\",\"Package\".\"SendTo\" FROM \"Include\" INNER JOIN \"Package\" ON (\"Package\".\"IncludeID\" = \"Include\".\"IncludeID\") WHERE \"Package\".\"PackageID\" = " + ID +";");
			while(rs.next()) {
				tofrom = "Lähettäjä: " + rs.getString("SendFrom") + "\nVastaanottaja: " + rs.getString("SendTo") + "\n";
				
				item1 = rs.getString("Item1");
				item2 = rs.getString("Item2");
				item3 = rs.getString("Item3");
				item4 = rs.getString("Item4");
				amount1 = rs.getString("Amount1");
				amount2 = rs.getString("Amount2");
				amount3 = rs.getString("Amount3");
				amount4 = rs.getString("Amount4");
			}
			
			if(item1 != null) {
				str += item1 + ", " + amount1 + " kpl \n"; 
				if(item2 != null) {
					str += item2 + ", " + amount2 + " kpl \n"; 
					if(item3 != null) {
						str += item3 + ", " + amount3 + " kpl \n"; 
						if(item4 != null) {
							str += item4 + ", " + amount4 + " kpl \n"; 
						}
					}
				}
			}
			info.add(tofrom);
			info.add(str);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return info;
	}
	
	//Print bill to "bill.txt"
	public void printBill(Connection con) {
		
		ResultSet rs;
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter("bill.txt"));
			
			out.write("*** KUITTI *** \n\n");
			out.write("Käyttäjätunnus: " + SPM.getUserName() + "\n");
			out.write("Päivämäärä: " + new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime()) + "\n");
			out.write("Kello: " + new SimpleDateFormat("HH.mm").format(Calendar.getInstance().getTime()) + "\n\n");
			out.write("Tämän istunnon aikana varastoon tallennetut paketit: " + "\n\n");
			
			ArrayList<String> storedPackages = SPM.getStoredPgThisSession();
			String s = "";
			int i = storedPackages.size();
			for(int j = 1; j <= i; j++) {
				s += "'" + storedPackages.get(j-1) + "'"; 
				if(j < i){
					s = s + ",";
				}
			}
			
			rs = con.createStatement().executeQuery("SELECT \"Package\".\"SendTo\",\"Route\".\"FromSmartPost\", \"Route\".\"ToSmartPost\",\"Item1\",\"Item2\",\"Item3\",\"Item4\",\"Amount1\",\"Amount2\",\"Amount3\",\"Amount4\" FROM \"Include\" INNER JOIN \"Package\" ON (\"Package\".\"IncludeID\" = \"Include\".\"IncludeID\") INNER JOIN \"Route\" ON (\"Route\".\"RouteID\" = \"Package\".\"RouteID\") WHERE \"Package\".\"PackageID\" IN (" + s + ");");
			String item1 = null, item2 = null, item3 = null, item4 = null, amount1 = null, amount2 = null, amount3 = null, amount4 = null;
			int k = 0;
			while(rs.next()) {
				out.write("\tKenelle: " + rs.getString("SendTo") + "\n");
				out.write("\tMistä: " + rs.getString("FromSmartPost") + "\n");
				out.write("\tMinne: " + rs.getString("ToSmartPost") + "\n");
				out.write("\tSisältö: ");
				item1 = rs.getString("Item1");
				item2 = rs.getString("Item2");
				item3 = rs.getString("Item3");
				item4 = rs.getString("Item4");
				amount1 = rs.getString("Amount1");
				amount2 = rs.getString("Amount2");
				amount3 = rs.getString("Amount3");
				amount4 = rs.getString("Amount4");
				
				if(item1 != null) {
					out.write(item1 + ", " + amount1 + " kpl\n"); 
					if(item2 != null) {
						out.write(" \t\t" + item2 + ", " + amount2 + " kpl \n"); 
						if(item3 != null) {
							out.write(" \t\t" + item3 + ", " + amount3 + " kpl \n"); 
							if(item4 != null) {
								out.write(" \t\t" + item4 + ", " + amount4 + " kpl \n"); 
							}
						}
					}
				}
				out.write("\n");
				k++;
			}
			
			out.write("Paketteja varastoitu tämän istunnun aikana: " + k + " kpl\n\n");
				
			out.write("Tämän istunnon aikana lähetetyt paketit: " + "\n\n");
			
			ArrayList<String> sentPackages = SPM.getSentPgThisSession();
			String str = "";
			int m = sentPackages.size();
			for(int n = 1; n <= m; n++) {
				str += "'" + sentPackages.get(n-1) + "'"; 
				if(n < m){
					str = str + ",";
				}
			}
			
			rs = con.createStatement().executeQuery("SELECT \"Package\".\"SendTo\",\"Route\".\"FromSmartPost\", \"Route\".\"ToSmartPost\",\"Item1\",\"Item2\",\"Item3\",\"Item4\",\"Amount1\",\"Amount2\",\"Amount3\",\"Amount4\", \"Log\".\"Fractured\", \"Log\".\"Successful\" FROM \"Include\" INNER JOIN \"Package\" ON (\"Package\".\"IncludeID\" = \"Include\".\"IncludeID\") INNER JOIN \"Route\" ON (\"Route\".\"RouteID\" = \"Package\".\"RouteID\") INNER JOIN \"Log\" ON (\"Log\".\"PackageID\" = \"Package\".\"PackageID\") WHERE \"Package\".\"PackageID\" IN (" + str + ");");
			int h = 0;
			while(rs.next()) {
				out.write("\tKenelle: " + rs.getString("SendTo") + "\n");
				out.write("\tMistä: " + rs.getString("FromSmartPost") + "\n");
				out.write("\tMinne: " + rs.getString("ToSmartPost") + "\n");
				String frac = rs.getString("Fractured");
				if(frac.equals("true")) {
					out.write("\tEsineitä hajosi toimituksessa.\n");
				}
				String succ = rs.getString("Successful");
				if(succ.equals("false")) {
					out.write("\tPaketti toimitettiin väärään osoitteeseen.\n");
				}
				out.write("\tSisältö: ");
				item1 = rs.getString("Item1");
				item2 = rs.getString("Item2");
				item3 = rs.getString("Item3");
				item4 = rs.getString("Item4");
				amount1 = rs.getString("Amount1");
				amount2 = rs.getString("Amount2");
				amount3 = rs.getString("Amount3");
				amount4 = rs.getString("Amount4");
				
				if(item1 != null) {
					out.write(item1 + ", " + amount1 + " kpl\n"); 
					if(item2 != null) {
						out.write(" \t\t" + item2 + ", " + amount2 + " kpl \n"); 
						if(item3 != null) {
							out.write(" \t\t" + item3 + ", " + amount3 + " kpl \n"); 
							if(item4 != null) {
								out.write(" \t\t" + item4 + ", " + amount4 + " kpl \n"); 
							}
						}
					}
				}
				out.write("\n");
				h++;
			}
			
			out.write("Paketteja varastoitu tämän istunnun aikana: " + h + " kpl\n\n");
			
			out.write("Tervetuloa käyttämään palveluamme uudelleen!");
			out.close();
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		
		
	}
	
	//Removing packages which are in warehouse
	public void emptyWarehouse(Connection con) {
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT * FROM \"Package\" INNER JOIN \"Warehouse\" ON (\"Package\".\"PackageID\" = \"Warehouse\".\"PackageID\");");
			while(rs.next()){
				con.createStatement().execute("DELETE FROM \"Package\" WHERE \"PackageID\" = " + rs.getInt("PackageID") + ";");
				con.createStatement().execute("DELETE FROM \"Warehouse\" WHERE \"PackageID\" = " + rs.getInt("PackageID") + ";");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//Get log information on specific date
	public ObservableList<ObservableList<String>> getLogDate(LocalDate ldate, Connection con) {
		
		ObservableList<ObservableList<String>> info = FXCollections.observableArrayList();

		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Log\".\"PackageID\",\"Route\".\"FromSmartPost\", \"Route\".\"ToSmartPost\", \"CreateDate\", \"Fractured\",\"Successful\" FROM \"Log\" INNER JOIN \"Package\" ON (\"Package\".\"PackageID\" = \"Log\".\"PackageID\") INNER JOIN \"Route\" ON (\"Package\".\"RouteID\" = \"Route\".\"RouteID\") WHERE \"SentDate\" = '" + ldate + "';");
			while(rs.next()) {
				String s2 = "";
				
				String date2 = rs.getString("CreateDate");
				String[] string2 = date2.split("-");
				s2 = string2[2] + "." + string2[1] + "." + string2[0];
				
				String[] str = new String[2];
				if(rs.getString("Fractured").equals("true")) {
					str[0] = "Rikki";
				}
				else {
					str[0] = "Ehjä";
				}
				if(rs.getString("Successful").equals("true")) {
					str[1] = "Onnistunut";
				}
				else {
					str[1] ="Väärä osoite";
				}
				info.add(FXCollections.observableArrayList(rs.getString("PackageID"),rs.getString("FromSmartPost"),rs.getString("ToSmartPost"), s2, str[0], str[1]));
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return info;
	}
	
	//Get warehouse information on specific date
	public ObservableList<ObservableList<String>> getWarehouseDate(LocalDate ldate, Connection con) {

		ObservableList<ObservableList<String>> info = FXCollections.observableArrayList();
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Package\".\"PackageID\",\"Route\".\"FromSmartPost\",\"Route\".\"ToSmartPost\" FROM \"Package\" INNER JOIN \"Warehouse\" ON (\"Package\".\"PackageID\" = \"Warehouse\".\"PackageID\")INNER JOIN \"Route\" ON (\"Package\".\"RouteID\" = \"Route\".\"RouteID\") WHERE \"CreateDate\" = '" + ldate + "';");
			while(rs.next()) {
				info.add(FXCollections.observableArrayList(rs.getString("PackageID"),rs.getString("FromSmartPost"),rs.getString("ToSmartPost")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return info;
	}
	
	//Get information about users sent packages
	public ObservableList<ObservableList<String>> getMySentInformation(Connection con) {
		
		ObservableList<ObservableList<String>> info = FXCollections.observableArrayList();
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Log\".\"PackageID\",\"Route\".\"FromSmartPost\", \"Route\".\"ToSmartPost\",\"SentDate\" FROM \"Log\" INNER JOIN \"Package\" ON (\"Package\".\"PackageID\" = \"Log\".\"PackageID\") INNER JOIN \"Route\" ON (\"Package\".\"RouteID\" = \"Route\".\"RouteID\") WHERE \"Package\".\"SendFrom\" = '" + SPM.getUserName() + "';");
			while(rs.next()) {
				String s2 = "";
				
				String date2 = rs.getString("SentDate");
				String[] string2 = date2.split("-");
				s2 = string2[2] + "." + string2[1] + "." + string2[0];
				
				info.add(FXCollections.observableArrayList(rs.getString("PackageID"),rs.getString("FromSmartPost"),rs.getString("ToSmartPost"), s2));
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return info;
	}
	
	//Get information about users received packages
	public ObservableList<ObservableList<String>> getMyReceivedInformation(Connection con) {
		
		ObservableList<ObservableList<String>> info = FXCollections.observableArrayList();
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Log\".\"PackageID\",\"Route\".\"FromSmartPost\", \"Route\".\"ToSmartPost\",\"SentDate\", \"Fractured\",\"Successful\" FROM \"Log\" INNER JOIN \"Package\" ON (\"Package\".\"PackageID\" = \"Log\".\"PackageID\") INNER JOIN \"Route\" ON (\"Package\".\"RouteID\" = \"Route\".\"RouteID\") WHERE \"Package\".\"SendTo\" = '" + SPM.getUserName() + "';");
			while(rs.next()) {
				String s2 = "";
				
				String date2 = rs.getString("SentDate");
				String[] string2 = date2.split("-");
				s2 = string2[2] + "." + string2[1] + "." + string2[0];
				
				String[] str = new String[2];
				if(rs.getString("Fractured").equals("true")) {
					str[0] = "Rikki";
				}
				else {
					str[0] = "Ehjä";
				}
				if(rs.getString("Successful").equals("true")) {
					str[1] = "Onnistunut";
				}
				else {
					str[1] ="Väärä osoite";
				}
				
				info.add(FXCollections.observableArrayList(rs.getString("PackageID"),rs.getString("FromSmartPost"),rs.getString("ToSmartPost"), s2, str[0], str[1]));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return info;
	}
		
	//Sets propersies for package tableview
	public void setBaseTable(ArrayList<String> columns,TableView<ObservableList<String>> table) {
		
		int i = 0;
		
		if(columns.contains("PackageID")) {
			addColumn(new TableColumn<>("ID"), i, table); 
		    i++;
		}
		
		if(columns.contains("From")) {
		    addColumn(new TableColumn<>("Mistä"), i, table);
		    i++;
		}

		if(columns.contains("To")) {
			addColumn(new TableColumn<>("Minne"), i, table);
		    i++;
		}
		
		if(columns.contains("CreateDate")) {
		    addColumn(new TableColumn<>("Varastoitu"), i, table);
		    i++;
		}
	    
		if(columns.contains("SentDate")) {
			addColumn(new TableColumn<>("Lähetetty"), i, table);
		    i++;
		}
		
		if(columns.contains("ReceiveDate")) {
			addColumn(new TableColumn<>("Vastaanotettu"), i, table);
		    i++;
		}

		if(columns.contains("Fractured")) {
			addColumn(new TableColumn<>("Sisältö"), i, table);
		    i++;
		}
		
		if(columns.contains("Successful")) {
			addColumn(new TableColumn<>("Toimitus"), i, table);
		    i++;
		}
	}

	//Adds table to tableview
	private void addColumn(TableColumn<ObservableList<String>, String> id, int i, TableView<ObservableList<String>> table) {
		
	    id.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList<String>,String>, ObservableValue<String>>() {         
	        @Override
	        public ObservableValue<String> call(CellDataFeatures<ObservableList<String>, String> cdf) {     
	            return new SimpleStringProperty(cdf.getValue().get(i));
	        }
	    }); 
	    
	    table.getColumns().add(id);
	}
	
		
}
