//Tekijä: Vilma Mäkitalo
//Päivämäärä: 16.06.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)

package Methods;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Item {

	private static Item item = null;
	
	private Item() {
		
	}
	
	static public Item getInstance() {
		
		if(item == null) {
			item = new Item();
		}
		return item;
	}
	
	//Create new window for creating new item
	public void createItemPopup() {
		
		try {
			Stage view = new Stage();		//Popup window for adding Item
			Parent page = FXMLLoader.load(getClass().getResource("/FXML/AddItemPopup.fxml"));
			Scene scene = new Scene(page);
			
			view.setScene(scene);
			scene.getStylesheets().add(getClass().getResource("/Css/AddItem.css").toExternalForm());
			view.showAndWait();;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Create popup for removing item
	public void removeItemPopup() {
		
		try {
			Stage view = new Stage();		//Popup window for removing Item
			Parent page = FXMLLoader.load(getClass().getResource("/FXML/RemoveItemPopup.fxml"));
			Scene scene = new Scene(page);
			
			view.setScene(scene);
			scene.getStylesheets().add(getClass().getResource("/Css/RemoveItem.css").toExternalForm());
			view.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Check, if given input is in correct form
	public ArrayList checkValues(String name, Object f, String s, String w) {
		
		ArrayList input = new ArrayList();
		
		try {
			if(name.equals("") || f.toString().equals("") || s.equals("") || w.equals("")) {
				return input;
			}
			
			int fragile = Integer.parseInt(f.toString());
			double size = Double.parseDouble(s);
			double weight = Double.parseDouble(w);
			
			input.add(name);
			input.add(fragile);
			input.add(size);
			input.add(weight);
			
		} catch (NullPointerException | NumberFormatException e) {
			e.printStackTrace();
			return new ArrayList<String>();
		}
		return input;
	}
	
	//Create new item
	public int createItem(ArrayList input, Connection con) {
		
		String name = input.get(0).toString();
		int fragile = Integer.parseInt(input.get(1).toString());
		double size = Double.parseDouble(input.get(2).toString());
		double weight = Double.parseDouble(input.get(3).toString());
		
		try {
			con.createStatement().execute("INSERT INTO \"Item\" VALUES('" + name + "'," + fragile + "," + size + "," + weight + ");");
		} catch (SQLException e) {
			e.printStackTrace();
			return 1;
		}
		return 0;
	}
	
	//Get items for combobox
	public ArrayList<String> getItems(Connection con) {
		
		ArrayList<String> items = new ArrayList<String>();
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Name\" FROM \"Item\";");
			while(rs.next()) {
				items.add(rs.getString("Name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return items;
	}
	
	//Get items fragile, size, weight
	public String getItemInfo(String i, Connection con) {
		
		ResultSet rs = null;
		String info = null;
		
		try {
			rs = con.createStatement().executeQuery("SELECT \"Fragile\",\"Size\",\"Weight\" FROM \"Item\" WHERE \"Name\" = '" + i + "';");
			while(rs.next()) {
				info = "Särkyvyys: " + rs.getInt("Fragile") + "\nKoko: " + rs.getDouble("Size") + "l\nPaino: " + rs.getDouble("Weight") + "g"; 
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return info;
	}
	
	//Create include for package
	public int createInclude(String item1, String item2, String item3, String item4, String a1, String a2, String a3, String a4, Connection con) {
		
		ArrayList<String> items = new ArrayList<String>();
		int i = 0;
		
		if(item1 != null){
			if(a1 != null){
				try {
					int amount1 = Integer.parseInt(a1); 
					if(amount1 > 0) {
						items.add(item1);
						items.add(a1);
						i++;
					}
				} catch (NumberFormatException e) {
					return 0;
				}
			}
		}
		if(item2 != null){
			if(a2 != null){
				try {
					int amount2 = Integer.parseInt(a2);
					if(amount2 > 0) {
						items.add(item2);
						items.add(a2);
						i++;
					}
				} catch (NumberFormatException e) {
					return 0;
				}
			}
		}
		if(item3 != null){
			if(a3 != null){
				try {
					int amount3 = Integer.parseInt(a3); 
					if(amount3 > 0) {
						items.add(item3);
						items.add(a3);
						i++;
					}
				} catch (NumberFormatException e) {
					return 0;
				}
			}
		}
		if(item4 != null){
			if(a4 != null){
				try {
					int amount4 = Integer.parseInt(a4);  
					if(amount4 > 0) {
						items.add(item4);
						items.add(a4);
						i++;
					}
				} catch (NumberFormatException e) {
					return 0;
				}
			}
		}
		if(items.isEmpty()) {
			return 0;
		}
		
		String s = "INSERT INTO \"Include\" (";
		for(int j = 1; j <= i; j++) {
			s += "\"item" + j + "\",\"Amount" + j + "\""; 
			if(j < i){
				s = s + ",";
			}
		}
		s = s + ") VALUES (";
		int h = 0;
		for(int j = 1; j <= i; j++) {
			s += "'" + items.get(h) + "'," + items.get(++h);
			h++;
			if(j < i){
				s = s + ",";
			}
		}
		s = s + ");";
		
		ResultSet rs;
		int id = 0;
		try {
			con.createStatement().execute(s);
			rs = con.createStatement().executeQuery("SELECT \"IncludeID\" FROM \"Include\";");
			while(rs.next()) {
				id = Integer.parseInt(rs.getString("IncludeID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return id;
		
	}
	
	//Get package's include's weight, size and the weakest part
	public ArrayList<String> getIncludeInfo(String item1, String item2, String item3, String item4, String a1, String a2, String a3, String a4, Connection con) {
		
		ArrayList<String> info = new ArrayList<String>();
		double weight = 0;
		double size = 0;
		int fragile = 10;
		String fragileP = null;
		
		if(item1 != null){
			if(a1 != null){
				ResultSet rs;
					try {
						rs = con.createStatement().executeQuery("SELECT \"Weight\",\"Size\",\"Fragile\" FROM \"Item\" WHERE \"Name\" = '" + item1 + "';");
						while(rs.next()) {
							weight += rs.getDouble("Weight") * Double.parseDouble(a1);
							size += rs.getDouble("Size") * Double.parseDouble(a1);
							if(fragile >= rs.getInt("Fragile")) {
								fragile = rs.getInt("Fragile");
								fragileP = item1 + ": " + rs.getInt("Fragile");
							}
						}
					} catch (SQLException e ) {
						e.printStackTrace();
					} catch (NumberFormatException e) {
						e.printStackTrace();
						return new ArrayList<String>();
					}
			}
		}
		if(item2 != null){
			if(a2 != null){
				ResultSet rs;
				try {
					rs = con.createStatement().executeQuery("SELECT \"Weight\",\"Size\",\"Fragile\" FROM \"Item\" WHERE \"Name\" = '" + item2 + "';");
					weight += rs.getDouble("Weight") * Double.parseDouble(a2);
					size += rs.getDouble("Size") * Double.parseDouble(a2);
					if(fragile >= rs.getInt("Fragile")) {
						fragile = rs.getInt("Fragile");
						fragileP = item2 + ": " + rs.getInt("Fragile");
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (NumberFormatException e) {
					e.printStackTrace();
					return new ArrayList<String>();
				}
			}
		}
		if(item3 != null){
			if(a3 != null){
				ResultSet rs;
				try {
					rs = con.createStatement().executeQuery("SELECT \"Weight\",\"Size\",\"Fragile\" FROM \"Item\" WHERE \"Name\" = '" + item3 + "' ;");
					weight += rs.getDouble("Weight") * Double.parseDouble(a3);
					size += rs.getDouble("Size") * Double.parseDouble(a3);
					if(fragile >= rs.getInt("Fragile")) {
						fragile = rs.getInt("Fragile");
						fragileP = item3 + ": " + rs.getInt("Fragile");
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (NumberFormatException e) {
					e.printStackTrace();
					return new ArrayList<String>();
				}
			}
		}
		if(item4 != null){
			if(a4 != null){
				ResultSet rs;
				try {
					rs = con.createStatement().executeQuery("SELECT \"Weight\",\"Size\",\"Fragile\" FROM \"Item\" WHERE \"Name\" = '" + item4 + "';");
					weight += rs.getDouble("Weight") * Double.parseDouble(a4);
					size += rs.getDouble("Size") * Double.parseDouble(a4);
					if(fragile >= rs.getInt("Fragile")) {
						fragile = rs.getInt("Fragile");
						fragileP = item4 + ": " + rs.getInt("Fragile");
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (NumberFormatException e) {
					e.printStackTrace();
					return new ArrayList<String>();
				}
			}
		}
		
		info.add(String.valueOf(weight) + "g");
		info.add(String.valueOf(size) + "l");
		info.add(fragileP);
		return info;
	}

	//Remove item
	public void removeItem(String name, Connection con) {
		
		try {
			con.createStatement().execute("DELETE FROM \"Item\" WHERE \"Name\" = '" + name + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	//Remove include
	public void removeInclude(int IncludeID, Connection con) {
		
		try {
			con.createStatement().execute("DELETE FROM \"Include\" WHERE \"IncludeID\" = " + IncludeID  + ";");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
