//Tekijä: Vilma Mäkitalo
//Päivämäärä: 16.06.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), fragile(1-10, 1=weak)


package Methods;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Main.SmartPostManager;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Route {

	private static Route route = null;
	private ArrayList<String> coordinates;
	private String fromPost;
	private String toPost;
	private static int RouteID;
	private static ArrayList<String> drawnRoutes;
	private static SmartPostManager SPM;
	
	private Route() {
		
	}
	
	static public Route getInstance() {
		
		if(route == null) {
			route = new Route();
			SPM = SPM.getInstance();
			RouteID = 0;
			drawnRoutes = new ArrayList<String>();
		}
		return route;
	}
	
	//Send coordinates between windows
	public void setCoordinates(ArrayList<String> coor) {
		coordinates = coor;
	}
	
	//Get last added route's start and end coordinates for drawing
	public ArrayList<String> getCoordinates() {
		return coordinates;
	}
	
	//Get last added routeID 
	public int getRouteID() {
		return RouteID;
	}
	
	//Set RouteID
	public void setRouteID(int i) {
		RouteID = i;
	}
	
	//Create new window for creating new route
	public void addRoutePopup() {
		
		try {
			
			Stage view = new Stage();		//Popup window for adding route
			Parent page = FXMLLoader.load(getClass().getResource("/FXML/AddRoutePopup.fxml"));
			Scene scene = new Scene(page);
			
			view.setScene(scene);
			scene.getStylesheets().add(getClass().getResource("/Css/AddRoute.css").toExternalForm());
			view.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Get routes coordinates for drawing the route
	public ArrayList<String> getCoordinates(String fromOffice, String toOffice, Connection con) {
		
		ArrayList<String> coor = new ArrayList<String>();
		
		if(fromOffice == null || toOffice == null || fromOffice.contains(toOffice) || toOffice.contains(fromOffice)) {
			return coor;
		}
		
		ResultSet rs, rs1;
		try {
			rs= con.createStatement().executeQuery("SELECT \"lat\",\"long\" FROM \"SmartPost\" WHERE \"Postoffice\" = '" + fromOffice + "';");
			
			while(rs.next()) {
				coor.add(Double.toString(rs.getDouble("lat")));
				coor.add(Double.toString(rs.getDouble("long")));
			}
			
			rs1 = con.createStatement().executeQuery("SELECT \"lat\",\"long\" FROM \"SmartPost\" WHERE \"Postoffice\" = '" + toOffice + "';");
			while(rs1.next()) {
				coor.add(Double.toString(rs1.getDouble("lat")));
				coor.add(Double.toString(rs1.getDouble("long")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coor;
	}
	
	//Create temporary route information without routes lenght
	public void createRoute(String fromOffice, String toOffice, Connection con) {
		
		
		ResultSet rs;
		try {
			con.createStatement().execute("INSERT INTO \"Route\"(\"FromSmartPost\",\"ToSmartPost\",\"Lenght\") VALUES ('" + fromOffice +  "', '" + toOffice + "',0);");
			fromPost = fromOffice;
			toPost = toOffice;
			int id = 0;
			rs = con.createStatement().executeQuery("SELECT \"RouteID\" FROM \"Route\";");
			while(rs.next()) {
				id = Integer.parseInt(rs.getString("RouteID").toString());
			}
			RouteID = id;
		} catch (SQLException e) {
			e.printStackTrace();
			int id = 0;
			try {
				rs = con.createStatement().executeQuery("SELECT \"RouteID\" FROM \"Route\" WHERE \"FromSmartPost\" = '" + fromOffice + "' AND \"ToSmartPost\" = '" + toOffice + "';");
				while(rs.next()) {
					id = Integer.parseInt(rs.getString("RouteID").toString());
				}
				RouteID = id;
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
		}
	}
	
	//Update temporary route to final form with lenght
	public void saveRoute(double lenght, Connection con) {
		
		try {
			con.createStatement().execute("UPDATE \"Route\"  SET \"lenght\" = " + lenght + " WHERE \"FromSmartPost\" = '" + fromPost + "' AND \"ToSmartPost\" = '" + toPost + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//Get route's lenght
	public String getLenght(int RouteID, Connection con) {
		
		ResultSet rs;
		String lenght = null;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Lenght\" FROM \"Route\" WHERE \"RouteID\" = '" + RouteID + "';");
			while(rs.next()) {
				lenght = String.valueOf(rs.getDouble("Lenght"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lenght;
	}
	
	//Create 'command' for drawing route on map
	public String drawRoute(Connection con) {
	
		String command = "document.createPath(";
		String wrongOffice = SPM.getWrongOffice();
		String toOffice = null, fromOffice = null;
		int packageID = SPM.getPackageID();
		ArrayList<String> coordinates;
		String pgClass = null;
		int packageClass;
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Route\".\"FromSmartPost\",\"Route\".\"ToSmartPost\", \"PackageClassName\" FROM \"Package\" INNER JOIN \"Route\" ON (\"Package\".\"RouteID\" = \"Route\".\"RouteID\") WHERE \"PackageID\" = " + packageID + ";");
			if(wrongOffice.equals("")) {
				while(rs.next()) {
					fromOffice = rs.getString("FromSmartPost");
					toOffice = rs.getString("ToSmartPost");
					pgClass = rs.getString("PackageClassName");
				}
			}
			else {
				while(rs.next()) {
					String[] s = wrongOffice.split(":");
					toOffice = s[1].trim();
					fromOffice =rs.getString("FromSmartPost");
					pgClass = rs.getString("PackageClassName");
				}
			}
			
			packageClass = Integer.parseInt(pgClass.replaceAll("[\\D]", ""));
			coordinates = getCoordinates(fromOffice,toOffice,con);
			
			command += coordinates + ", 'red', " + packageClass + ")";
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return command;
	}

	//Emptys drawnroutes list
	public void emptyDrawnRoutes() {
		drawnRoutes.clear();
	}
	
	//Adds route to drawnroutes list
	public void addToDrawnRoutes(String s) {
		drawnRoutes.add(s);
	}
	
	//Gets drawnroutes list
	public ArrayList<String> getDrawnRoutes() {
		return drawnRoutes;
	}
}
