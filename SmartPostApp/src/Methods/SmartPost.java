//Tekijä: Vilma Mäkitalo
//Päivämäärä: 16.06.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)


package Methods;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SmartPost {
	
	private static SmartPost sp = null;
	
	private SmartPost() {
	}

	static public SmartPost getInstance() {
		
		if(sp == null) {
			sp = new SmartPost();
		}
		return sp;
	}
	
	//Get SmartPost citys for combobox
	public ArrayList<String> getSmartPostCities(Connection con) {
		
		ArrayList<String> cities = new ArrayList<String>();
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"City\" FROM \"SmartPost\" GROUP BY \"City\";");
			while(rs.next()) {
				cities.add(rs.getString("City"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cities;
	}
	
	//Get SmartPost citys and offices for comboboxes
	public ArrayList<String> getSPCitiesOffices(Connection con) {
		
		ArrayList<String> citiesoffices = new ArrayList<String>();
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"City\",\"Postoffice\" FROM \"SmartPost\";");
			while(rs.next()) {
				String s = rs.getString("City") + ": " + rs.getString("Postoffice");
				citiesoffices.add(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return citiesoffices;
	}
	
	//Get City's SmartPosts' addresses and availabilities for map
	public ArrayList<String> getDrawInformation(String city, Connection con) {
		
		ArrayList<String> information = new ArrayList<String>();
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Address\",\"Availability\" FROM \"SmartPost\" WHERE \"City\" = '" + city + "';");
			while(rs.next()) {
				String info = rs.getString("Address") + ", " + city + ":" + rs.getString("Availability");
				information.add(info);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return information;
	}
	
	//Get City's SmartPost offices for combobox
	public ArrayList<String> getOffices(String city, Connection con) {
		
		ArrayList<String> offices = new ArrayList<String>();
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Postoffice\" FROM \"SmartPost\" WHERE \"City\" = '" + city + "';");
			while(rs.next()) {
				offices.add(rs.getString("Postoffice"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return offices;
	}
	
}
