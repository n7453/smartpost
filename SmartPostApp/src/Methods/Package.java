//Tekijä: Vilma Mäkitalo
//Päivämäärä: 16.06.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), fragile(1-10, 1=weak)


package Methods;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import javax.swing.table.TableModel;

import Main.SmartPostManager;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;

public class Package {
	
	private static Package pg = null;
	private static SmartPostManager SPM;
	private String sendMessage;
	private int packageID;
	private String wrongOffice;
	private ArrayList<String> storedPackages = new ArrayList<String>();
	private ArrayList<String> sentPackages = new ArrayList<String>();
	
	private Package() {
		
	}
	
	public static Package getInstance() {
		
		if(pg == null) {
			pg = new Package();
			SPM = SPM.getInstance();
		}
		return pg;
	}
	
	//Get information which tells if package is sented in correct smartpost and if something has fractured
	public String getSendMessage() {
		return sendMessage;
	}
	
	//Get last added packageID
	public int getPackageID() {
		return packageID;
	}
	
	//If package was sented to wrong smartpost, gives that smartpost other wise gives empty string
	public String getWrongOffice() {
		return wrongOffice;
	}
	
	//Create new window for creating new package
	public void createPgPopupWindow() {
		
		try {
			
			Stage view = new Stage();		//Popup window for adding package
			Parent page = FXMLLoader.load(getClass().getResource("/FXML/AddPackagePopup.fxml"));
			Scene scene = new Scene(page);
			
			view.setScene(scene);
			scene.getStylesheets().add(getClass().getResource("/Css/AddPackage.css").toExternalForm());
			view.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Create new window for sending package
	public void sendPgPopupWindow() {
		
		try {
			
			Stage view = new Stage();		//Popup window for sending package
			Parent page = FXMLLoader.load(getClass().getResource("/FXML/SendPackagePopup.fxml"));
			Scene scene = new Scene(page);
			
			view.setScene(scene);
			scene.getStylesheets().add(getClass().getResource("/Css/SendPackage.css").toExternalForm());
			view.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	//Get packageclasses' names
	public ArrayList<String> getPackageClass(Connection con) {
		
		ArrayList<String> packageClass = new ArrayList<String>();
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Name\" FROM \"PackageClass\";");
			while(rs.next()) {
				packageClass.add(rs.getString("Name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return packageClass;
	}
	
	//Get chosen packageclass's reliability/fragile, maxweight, maxsize and maxdistance
	public ArrayList<String> getPackageClassInfo(String pgc, Connection con) {
		
		ArrayList<String> packageClassInfo = new ArrayList<String>();
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"MaxWeight\",\"MaxSize\",\"Fragile\",\"MaxDistance\" FROM \"PackageClass\" WHERE \"Name\" = \"" + pgc + "\";");
			while(rs.next()) {
				if(rs.getDouble("MaxWeight") == 0) {
					packageClassInfo.add("Ei rajoitettu");
				}
				else {
					packageClassInfo.add(String.valueOf(rs.getDouble("MaxWeight") + "g"));
				}
				if(rs.getDouble("MaxSize") == 0) {
					packageClassInfo.add("Ei rajoitettu");
				}
				else {
					packageClassInfo.add(String.valueOf(rs.getDouble("MaxSize") + "l"));
				}
				if(rs.getDouble("MaxDistance") == 0) {
					packageClassInfo.add("Ei rajoitettu");
				}
				else {
					packageClassInfo.add(String.valueOf(rs.getDouble("MaxDistance") + "km"));
				}
				packageClassInfo.add(String.valueOf(rs.getInt("Fragile")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return packageClassInfo;
	}
	
	//Check, if chosen include could be sented in chosen packageclass
	public String checkPackage(String nw, String ns, String nf, String nl, String cw, String cs, String cf, String cl) {
		
		String info = "";
		double newWeight, newSize, classWeight, classSize, newLenght, classLenght;
		String[] f;
		int newFragile, classFragile;
		try {
			newWeight = Double.parseDouble(nw.replace("g", ""));
			newSize = Double.parseDouble(ns.replace("l", ""));
			newLenght = Double.parseDouble(nl.replace("km", ""));
			f = nf.split(":");
			newFragile = Integer.parseInt(f[1].trim());
			
			if(!cw.equals("") && !cs.equals("") && !cf.equals("") && !cl.equals("")) {
				if(!cw.contains("Ei rajoitettu")){
					classWeight = Double.parseDouble(cw.replace("g", ""));
					if(newWeight > classWeight) {
						info = info + "1";
					}
				}
				if(!cs.contains("Ei rajoitettu")) {
					classSize = Double.parseDouble(cs.replace("l", ""));
					if(newSize > classSize) {
						info = info + "2";
					}	
				}
				if(!cl.contains("Ei rajoitettu")) {
					classLenght = Double.parseDouble(cl.replace("km", ""));
					if(newLenght > classLenght) {
						info = info + "4";
					}
				}
				classFragile = Integer.parseInt(cf);
				
				if(newFragile <= 5 && classFragile <= 5 ) {
					info = info + "3";
				}
			}
			else {
				info = "5";
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return "0";
		}
		
		return info;
	}
	
	//Save package to warehouse 
	public void saveToWarehouse(String toName,int RouteID, String pgClass, int IncludeID, Connection con) {
		
		ResultSet rs;
		int ID = 0;
		try {
			con.createStatement().execute("INSERT INTO \"Package\" (\"RouteID\",\"PackageClassName\",\"IncludeID\",\"SendFrom\",\"SendTo\") VALUES (" + RouteID + ",'" + pgClass + "'," + IncludeID +",'" + SPM.getUserName() + "','" + toName + "');");
			rs = con.createStatement().executeQuery("SELECT \"PackageID\" FROM \"Package\";");
			while(rs.next()) {
				ID = rs.getInt("PackageID");
			}
			String CreateDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
			con.createStatement().execute("INSERT INTO \"Warehouse\" (\"PackageID\",\"CreateDate\") VALUES (" + ID + ",'" + CreateDate +"');");
			storedPackages.add(String.valueOf(ID));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//Get packages which are in warehouse for showing them Observable
	public ObservableList<ObservableList<String>> getObPgFromWarehouse(Connection con) {
	
		ObservableList<ObservableList<String>> info = FXCollections.observableArrayList();
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Package\".\"PackageID\",\"Route\".\"FromSmartPost\",\"Route\".\"ToSmartPost\",\"Warehouse\".\"CreateDate\" FROM \"Package\" INNER JOIN \"Warehouse\" ON (\"Package\".\"PackageID\" = \"Warehouse\".\"PackageID\")INNER JOIN \"Route\" ON (\"Package\".\"RouteID\" = \"Route\".\"RouteID\");");
			while(rs.next()) {
				String s = "";
				String date = rs.getString("CreateDate");
				String[] string = date.split("-");
				s += string[2] + "." + string[1] + "." + string[0];
			
				info.add(FXCollections.observableArrayList(rs.getString("PackageID"),rs.getString("FromSmartPost"),rs.getString("ToSmartPost"),s));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return info;
	}
	
	//Send package
	public void sendPackage(String id, Connection con) {
		
		Boolean Fractured = false;
		Boolean Successful = true;
		String sentdate, createdate = null, item1 = null, item2 = null, item3 = null, item4 = null, broken = "";
		int fragile1 = 0, classfragile = 0, fragile2 = 0, fragile3 = 0, fragile4 = 0;
		ResultSet rs;
		
		sendMessage = "";
		wrongOffice = "";
		
		String[] s = id.split(",");
		int ID = Integer.parseInt(s[0].replaceAll("[\\D]", ""));
		packageID = ID;
		
		try {
			rs = con.createStatement().executeQuery("SELECT \"CreateDate\" FROM \"Warehouse\" WHERE \"PackageID\" = " + ID + ";");
			while(rs.next()) {
				createdate = rs.getString("CreateDate");
			}
			
			con.createStatement().execute("DELETE FROM \"Warehouse\" WHERE \"PackageID\" = " + ID + ";");
			sentPackages.add(String.valueOf(ID));
			
			sentdate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
			
			if(new Random().nextInt(10) == 5) {
				ArrayList<String> randOffice = SPM.getSPCitiesOffices(con);
				wrongOffice = randOffice.get(new Random().nextInt(randOffice.size()));
				sendMessage = "Paketti toimitettiin väärään automattiin. \nPaketti on noudettavissa automaatista: \n" + wrongOffice;
				Successful = false;
			}
			
			rs = con.createStatement().executeQuery("SELECT \"PackageClass\".\"Fragile\",\"Item1\",\"Item2\",\"Item3\",\"Item4\" FROM \"Include\" INNER JOIN \"Package\" ON (\"Package\".\"IncludeID\" = \"Include\".\"IncludeID\")INNER JOIN \"PackageClass\" ON (\"Package\".\"PackageClassName\" = \"PackageClass\".\"Name\") WHERE \"Package\".\"PackageID\" = " + ID + ";");
			while(rs.next()) {
				classfragile = rs.getInt("Fragile");
				item1 = rs.getString("Item1");
				item2 = rs.getString("Item2");
				item3 = rs.getString("Item3");
				item4 = rs.getString("Item4");
			}
			
			if(item1 != null) {
				rs = con.createStatement().executeQuery("SELECT \"Fragile\" FROM \"Item\" WHERE \"Name\" = '" + item1 + "';");
				while(rs.next()) {
					fragile1 = rs.getInt("Fragile");
				}
				if(item2 != null) {
					rs = con.createStatement().executeQuery("SELECT \"Fragile\" FROM \"Item\" WHERE \"Name\" = '" + item2 + "';");
					while(rs.next()) {
						fragile2 = rs.getInt("Fragile");
					}
					if(item3 != null) {
						rs = con.createStatement().executeQuery("SELECT \"Fragile\" FROM \"Item\" WHERE \"Name\" = '" + item3 + "';");
						while(rs.next()) {
							fragile3 = rs.getInt("Fragile");
						}
						if(item4 != null) {
							rs = con.createStatement().executeQuery("SELECT \"Fragile\" FROM \"Item\" WHERE \"Name\" = '" + item4 + "';");
							while(rs.next()) {
								fragile4 = rs.getInt("Fragile");
							}
						}
					}
				}
			}
			ArrayList<String> brokenItems = new ArrayList<String>();
			if((new Random().nextInt(classfragile) + 1) == classfragile) {
				if(fragile1 != 0 && (new Random().nextInt(fragile1) + 1) == fragile1) {
					Fractured = true;
					brokenItems.add(item1);
				}
				if(fragile2 != 0 && (new Random().nextInt(fragile2) + 1) == fragile2) {
					Fractured = true;
					brokenItems.add(item2);
				}
				if(fragile3 != 0 && (new Random().nextInt(fragile3) + 1) == fragile3) {
					Fractured = true;
					brokenItems.add(item3);
				}
				if(fragile4 != 0 && (new Random().nextInt(fragile4) + 1) == fragile4) {
					Fractured = true;
					brokenItems.add(item4);
				}	
			}
			int i = brokenItems.size();
			for(int j = 1; j <= i; j++) {
				broken += brokenItems.get(j-1); 
				if(j < i){
					broken +=", ";
				}
			}
			if(!broken.equals("")) {
				sendMessage += "\nSeuraavat esineet hajosivat kuljetuksessa: " + broken;
			}
			
			if(sendMessage.equals("")) {
				sendMessage = "Paketti on toimitettu onnistuneesti.";
			}
			else {
				sendMessage += "\nPahoittelemme epäonnistumistamme. \nLähetämme tikkarin hyvityksenä.";
			}
			
			con.createStatement().execute("INSERT INTO \"Log\" (\"PackageID\",\"Successful\",\"Fractured\",\"CreateDate\",\"SentDate\") VALUES (" + ID + ", '" + Successful + "', '" + Fractured + "', '" + createdate + "', '" + sentdate + "');");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//Resets lists that keep track sent and stored packages
	public void resetPgCounts() {
		storedPackages.clear();
		sentPackages.clear();
	}
	
	//Gets pagkages' IDs which are stored in this session
	public ArrayList<String> getStoredPgThisSession() {
		return storedPackages;
	}
	
	//Gets pagkages' IDs which are sent in this session
	public ArrayList<String> getSentPgThisSession() {
		return sentPackages;
	}

	//Get users packages from warehouse for showing them Observable
	public ObservableList<ObservableList<String>> getUserPgFromWarehouse(String user, Connection con) {
		
		ObservableList<ObservableList<String>> info = FXCollections.observableArrayList();
		
		ResultSet rs;
		try {
			rs = con.createStatement().executeQuery("SELECT \"Package\".\"PackageID\",\"Route\".\"FromSmartPost\",\"Route\".\"ToSmartPost\",\"Warehouse\".\"CreateDate\" FROM \"Package\" INNER JOIN \"Warehouse\" ON (\"Package\".\"PackageID\" = \"Warehouse\".\"PackageID\")INNER JOIN \"Route\" ON (\"Package\".\"RouteID\" = \"Route\".\"RouteID\") WHERE \"SendFrom\" = '" + user + "';");
			while(rs.next()) {
				String s = "";
				String date = rs.getString("CreateDate");
				String[] string = date.split("-");
				s += string[2] + "." + string[1] + "." + string[0];
			
				info.add(FXCollections.observableArrayList(rs.getString("PackageID"),rs.getString("FromSmartPost"),rs.getString("ToSmartPost"),s));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return info;
	}
}
