//Tekijä: Vilma Mäkitalo
//Päivämäärä: 16.06.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)


package Main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import Methods.Item;
import Methods.Log;
import Methods.Package;
import Methods.Route;
import Methods.Safety;
import Methods.SmartPost;
import Methods.XMLReader;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;

public class SmartPostManager {
	
	private static SmartPostManager SPM = null;
	private SmartPost sp;
	private Route route;
	private Item item;
	private Package pg;
	private XMLReader xmlr;
	private Log log;
	private Safety safe;
	
	private int i;

	////Basic methods////
	
	private SmartPostManager() {
		sp = sp.getInstance();
		route = route.getInstance();
		item = item.getInstance();
		pg = pg.getInstance();
		xmlr = xmlr.getInstance();
		log = log.getInstance();
		safe = safe.getInstance();
	}
	
	static public SmartPostManager getInstance() {
		if (SPM == null) {
			SPM = new SmartPostManager();
			return SPM;
		}
		return SPM;
	}
	
	//Creating connection to database
	public Connection getDataBase(String dbName) {
		//Input: database's name
		//Output: connection to database
		
		Connection con = null;
		
		try {
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:" + dbName);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return con;
	}
	
	//Sending information between popup windows//	
	//Send/set order
	public void setI(int j) {
		i = j;
	}
	
	//Get/read order
	public int getI() {
		
		if(i == 1){
			i = 0;
			return 1;
		}
		if(i == 2){
			i = 0;
			return 2;
		}
		return 0;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	////SmartPost methods////
	
	//Get SmartPost citys for combobox
	public ArrayList<String> getSmartPostCities(Connection con) {
		//Input: Connection to database
		//Output: Arraylist of cities which have SmartPosts
		return sp.getSmartPostCities(con);
	}
	
	//Get SmartPost citys and offices for comboboxes
	public ArrayList<String> getSPCitiesOffices(Connection con) {
		//Input: Connection to database
		//Output: Arraylist of offices which have SmartPosts(City : postoffice)
		return sp.getSPCitiesOffices(con);
	}

	//Get City's SmartPosts' addresses and availabilities for map
	public ArrayList<String> getDrawInformation(String city, Connection con) {
		//Input: City which posts' info are wanted, Connection to database
		//Output: Arraylist of SmartPosts' addresses and availabilities(address,city : availability)
		return sp.getDrawInformation(city, con);
	}
	
	//Get City's SmartPost offices for combobox
	public ArrayList<String> getOffices(String city, Connection con) {
		//Input: City which posts are wanted, Connection to database
		//Output: ArrayList of posts' postoffices
		return sp.getOffices(city,con);
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	////Route methods////
	
	//Create new window for creating new route
	public void addRoutePopup() {
		route.addRoutePopup();
	}
	
	//Get last added route's start and end coordinates for drawing
	public ArrayList<String> getCoordinates() {
		//Output: Arraylist of coordinates([start lat, start long, end lat, end long])
		return route.getCoordinates();
	}
	
	//Send coordinates between windows
	public void setCoordinates(ArrayList<String> coor) {
		//Input: ArrayList of coordinates([start lat, start long, end lat, end long]) which is wanted to deliver to another controller
		route.setCoordinates(coor);
	}
	
	//Update temporary route to final form with length
	public void saveRoute(double d, Connection con) {
		//Input: Route's length, Connection to database
		route.saveRoute(d,con);
	}
	
	//Get last added routeID 
	public int getRouteID() {
		//Output: last added route's ID
		return route.getRouteID();
	}
	
	//Set routeID
	public void setRouteID(int i) {
		//Input: Route's ID
		route.setRouteID(i);
	}
	
	//Get route's length
	public String getRouteLenght(int RouteID, Connection con) {
		//Input: RouteID of route which length we want get, Connection to database
		//Output: Length of route
		return route.getLenght(RouteID,con);
	}
	
	//Create 'command' for drawing route on map
	public String drawRoute(Connection con) {
		//Input: Connection to database
		//Output: line/order for drawing route with javascript
		return route.drawRoute(con);
	}
	
	//Get routes coordinates for drawing the route
	public ArrayList<String> getCoordinates(String fromOffice, String toOffice, Connection con){
		//Input: From postoffice, to postoffice, connection to database
		//Output: Arraylist of coordinates([start lat, start long, end lat, end long])
		return route.getCoordinates(fromOffice,toOffice,con);
	}
	
	//Create temporary route information without routes lenght
	public void createRoute(String fromOffice, String toOffice, Connection con) {
		//Input: From office, to office, connection to database
		route.createRoute(fromOffice, toOffice, con);
	}
	
	//Empties drawnroutes list
	public void emptyDrawnRoutes() {
		route.emptyDrawnRoutes();
	}
	
	//Adds route to drawnroutes list
	public void addToDrawnRoutes(String s) {
		//Input: line/order for drawing route with javascript
		route.addToDrawnRoutes(s);
	}
	
	//Gets drawnroutes list
	public ArrayList<String> getDrawnRoutes() {
		//Output: Arraylist of string which have lines for drawing routes with javascript
		return route.getDrawnRoutes();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////77//////////////
	
	////Item methods including include methods////
	
	//Create new window for creating new item
	public void createItemPopup() {
		item.createItemPopup();
	}
	
	//Get items for combobox
	public ArrayList<String> getItems(Connection con) {
		//Input: Connection to database
		//Output: Arraylist of items which are in database
		return item.getItems(con);
	}
	
	//Get items fragile, size, weight
	public String getItemInfo(String i, Connection con) {
		//Input: Items name, Connection for database
		//Output: Items info ready to print
		return item.getItemInfo(i, con);
	}
	
	//Get package's include's weight, size and the weakest part
	public ArrayList<String> getIncludeInfo(String item1, String item2, String item3, String item4, String a1, String a2, String a3, String a4, Connection con) {
		//Input: Four possible items or empty strings, four amounts or empty strings(must mach with items. item1's amount is a1 etc.), Connection con
		//Output: Arraylist [weight, size, fragile]
		return item.getIncludeInfo(item1, item2, item3, item4, a1, a2, a3, a4, con);
	}
	
	//Create include for package
	public int createInclude(String item1, String item2, String item3, String item4, String a1, String a2, String a3, String a4, Connection con) {
		//Input: Four possible items or empty strings, four amounts or empty strings(must mach with items. item1's amount is a1 etc.), Connection con
		//Output: returns new includes ID or 0 if there is no inputs 
		return item.createInclude(item1, item2, item3, item4, a1, a2, a3, a4, con);
	}
	
	//Check, if given input is in correct form
	public ArrayList checkValues(String name, Object f, String s, String w) {
		//Input: Values for new item (name, fragile,size,weight)
		//Output: if values are in correct form, returns values in arraylist. Else return empty arraylist
		return item.checkValues(name, f, s, w);
	}
	
	//Create new item
	public int createItem(ArrayList input, Connection con) {
		//Input: Arraylist of values(name, fragile,size,weight), connection to database
		//Output: returns 0 if creating item went ok else return 1.
		return item.createItem(input, con);
	}
	
	//Create popup for removing items
	public void removeItemPopup() {
		item.removeItemPopup();
	}

	//Remove item
	public void removeItem(String name, Connection con) {
		//Input: name of item wanted to be removed, connection database
		item.removeItem(name,con);
	}
	
	//Remove include
	public void removeInclude(int IncludeID, Connection con) {
		//Input: Include's ID, connection to database
		item.removeInclude(IncludeID,con);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	////Package methods including packageclass and warehouse methods////
	
	//Create new window for creating new package
	public void createPackage() {
		pg.createPgPopupWindow();
	}
	
	//Get packageclasses' names
	public ArrayList<String> getPackageClass(Connection con) {
		//Input: Connection to database
		//Output: Arraylist of package class names
		return pg.getPackageClass(con);
	}
	
	//Get chosen packageclass's reliability/fragile, maxweight, maxsize and maxdistance
	public ArrayList<String> getPackageClassInfo(String pgc, Connection con) {
		//Input: Packageclass name, connection to database
		//Output: Arraylist of values([fragile, maxweight,maxsize,maxdistance])
		return pg.getPackageClassInfo(pgc,con);
	}
	
	//Check, if chosen include could be sent in chosen packageclass
	public String checkPackage(String nw, String ns, String nf, String nl, String cw, String cs, String cf, String cl) {
		//Input: packages info(weight,size,fragile), routes lenght, packageclass info(weiht,size,fragile,lenght)
		//Output: if string is empty, package is ok. Else string contains numbers which all mean different reason for package not being ok
		return pg.checkPackage(nw,ns,nf,nl,cw,cs,cf,cl);
	}
	
	//Save package to warehouse 
	public void saveToWarehouse(String toName,int RouteID, String pgClass, int IncludeID, Connection con) {
		//Input: who package is sented, route's ID, packageclass name, include's ID, connection to database
		pg.saveToWarehouse(toName,RouteID,pgClass,IncludeID, con);
	}
	
	//Create new window for sending package
	public void sendPackagePopup() {
		pg.sendPgPopupWindow();
	}
	
	//Get packages which are in warehouse for showing them in table
	public ObservableList<ObservableList<String>> getObPgFromWarehouse(Connection con) {
		//Input: Connection to database
		//Output: Observablelist of package info(PackageId, from postoffice, to postoffice, create date) 
		return pg.getObPgFromWarehouse(con);
	}
	
	//Send package
	public void sendPackage(String ID, Connection con) {
		//Input: Packages ID, connection to database
		pg.sendPackage(ID,con);
	}
	
	//Get information which tells if package is sent in correct smartpost and if something has fractured
	public String getSendMessage() {
		//Output: String ready to print
		return pg.getSendMessage();
	}
	
	//Get last added packageID
	public int getPackageID() {
		//Output: Packages ID
		return pg.getPackageID();
	}
	
	//If package was sent to wrong SmartPost, gives that SmartPost other wise gives empty string
	public String getWrongOffice() {
		//Output: If package was sent to wrong SmartPost, returns that SmartPost other wise returns empty string
		return pg.getWrongOffice();
	}
	
	//Resets lists that keep track sent and stored packages
	public void resetPgCounts() {
		pg.resetPgCounts();
	}
	
	//Gets pagkages' IDs which are stored in this session
	public ArrayList<String> getStoredPgThisSession() {
		//Output: Arraylist of packages' IDs in storepackagelist
		return pg.getStoredPgThisSession();
	}
	
	//Gets pagkages' IDs which are sent in this session
	public ArrayList<String> getSentPgThisSession() {
		//Output: Arraylist of packages' IDs in sentpackagelist
		return pg.getSentPgThisSession();
	}
	
	//Get users sent or received packages from warehouse for showing them in table
	public ObservableList<ObservableList<String>> getUserPgFromWarehouse(String user, Connection con) {
		//Input: users name, Connection to database
		//Output: Observablelist of package info(PackageId, from postoffice, to postoffice, create date) 
		return pg.getUserPgFromWarehouse(user,con);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////7//
	
	//XML Reading method
	public void readXML(String url, Connection con) {
		//Input: Url for the xml data, connection to database
		xmlr.readXML(url, con);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	////Log and warehouse methods////
	
	//Get information from log for showing them in table
	public ObservableList<ObservableList<String>> getLogInformation(Connection con) {
		//Input: users name, Connection to database
		//Output: Observablelist of package info(PackageId, from postoffice, to postoffice, sent date, fractured, successful) 
		return log.getLogInformation(con);
	}
	
	//Count how many packages are in warehouse
	public String getWarehouseSize(Connection con){
		//Input: Connection to database
		//Output: How many packages are in warehouse
		return log.getWarehouseSize(con);
	}
	
	//Count how many packages have been sent
	public String getLogSize(Connection con){
		//Input: Connection to database
		//Output: how many packages have been sent
		return log.getLogSize(con);
	}
	
	//Get information from package
	public ArrayList<String> getPackageInformation(String line, Connection con) {
		//Input: Line which has PackageID before first ",", Connection to database
		//Output: ArrayList of packages info ([send from, send to, items])
		return log.getPackageInformation(line, con);
	}
	
	//Print bill to "bill.txt"
	public void printBill(Connection con) {
		//Input: Connection to database
		log.printBill(con);
	}
		
	//Popup for asking if reset warehouse or continue with old information
	public void askWarehouseReset() {
		log.askWarehouseReset();
	}
	
	//Removing packages which are in warehouse
	public void emptyWarehouse(Connection con) {
		//Input: Connection to database
		log.emptyWarehouse(con);
	}
	
	//Get log information on specific date
	public ObservableList<ObservableList<String>> getLogDate(LocalDate date, Connection con) {
		//Input: chosen date, Connection to database
		//Output: Observablelist of package info(PackageId, from postoffice, to postoffice, create date, fractured, successful) 
		return log.getLogDate(date,con);
	}
	
	//Get warehouse information on specific day
	public ObservableList<ObservableList<String>> getWarehouseDate(LocalDate date, Connection con) {
		//Input: chosen date, Connection to database
		//Output: Observablelist of package info(PackageId, from postoffice, to postoffice) 
		return log.getWarehouseDate(date,con);
	}
	
	//Sets columns for package tableview from log
	public void setBaseTable(ArrayList<String> columns, TableView table) {
		//Input: Arraylist of wanted columns(much match ones in the method), table which wanted to be filled
		log.setBaseTable(columns, table);
	}
	
	//Get information about users sent packages
	public ObservableList<ObservableList<String>> getMySentInformation(Connection con) {
		//Input: chosen date, Connection to database
		//Output: Observablelist of package info(PackageId, from postoffice, to postoffice, sent date) 
		return log.getMySentInformation(con);
	}
	
	//Get information about users received packages
	public ObservableList<ObservableList<String>> getMyReceivedInformation(Connection con) {
		//Input: chosen date, Connection to database
		//Output: Observablelist of package info(PackageId, from postoffice, to postoffice, sent date, fractured, successful) 
		return log.getMyReceivedInformation(con);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	////Safety and user methods////
	
	//Popup for loging in or signing in
	public void logInPopup() {
		safe.logInPopup();
	}
	
	//Check username and password, and log in if ok
	public int logIn(String user, String password, Connection con) {
		//Input: username, password, connection to database
		//Output: returns 1 if log in successed else 0
		return safe.logIn(user,password,con);
	}
	
	//Get current username
	public String getUserName() {
		//Output: current user's username
		return safe.getUserName();
	}
	
	//Sign in as host
	public int signInHost(String name, String userName, String password, String dPassword, String safeCode, Connection con) {
		//Input: realname, username, password,password varify,safetycode, connection to database
		//Output: returns 4 if sign in successed else some other number which indicates different problems
		return safe.signInHost(name,userName,password,dPassword,safeCode,con);
	}
	
	//Sign in as host
	public int signInUser(String name, String userName, String password, String dPassword, Connection con) {
		//Input: realname, username, password,password varify, connection to database
		//Output: returns 3 if sign in successed else some other number which indicates different problems
		return safe.signInUser(name,userName,password,dPassword,con);
	}
	
	//Get current securityLevel
	public int getSecurityLevel() {
		//Output: current securitylevel(1 or 2)
		return safe.getSecurityLevel();
	}
	
	//Removes old safetycode from database
	public void updateSafetyCode(Connection con) {
		//Input: Connection to database
		safe.updateSafetyCode(con);
	}
	
	//Adds new safetycode to database
	public int addSafetyCode(String code, Connection con) {
		//Input: new code, connection to database
		//Output: returns 0 if code is ok, 1 if code is in wrong format and 2 if it already exists
		return safe.addSafetyCode(code,con);
	}
	
	//Set current username
	public void setUser(String name) {
		//Input: User's username
		safe.setUser(name);
	}

	//Get registered users, no hosts 
	public ArrayList<String> getUsers(Connection con) {
		//Input: Connection to database
		//Output: Arraylist of users' usernames
		return safe.getUsers(con);
	}
	
	//Get users name
	public String getUserRealName(String user, Connection con){
		//Input: User's username, connection to database
		//Output: user's realname
		return safe.getUserRealName(user, con);
	}

	//Removes account. User or host
	public int removeAccount(Connection con) {
		//Input: connection to database
		//Output: 0 if removing is ok, 1 if user is last host user
		return safe.removeAccount(con);
	}
}
