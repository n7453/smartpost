//Tekijä: Vilma Mäkitalo
//Päivämäärä: 10.07.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)

package Controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

import Main.SmartPostManager;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

public class RemoveItemPopupController implements Initializable{

	@FXML
	private Button removeItemButton;
	@FXML
	private ListView<String> itemsList;
	@FXML
	private Label textLabel;
	
	SmartPostManager SPM;
	Connection con;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		SPM = SPM.getInstance();
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
		
		itemsList.setItems(FXCollections.observableArrayList(SPM.getItems(con)));
		
	}

	@FXML
	public void showWarningAction() {
		
		textLabel.setText("Esineen poistaminen poistaa tiedon \nkaikista esineen sisältävistä/sisältäneistä \npaketeista.\nHaluatko varamasti poistaa tämän esineen?");
	}
	
	@FXML
	public void removeItemAction() {
		
		SPM.removeItem(itemsList.getSelectionModel().getSelectedItem().toString(), con);
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Stage stage = (Stage) removeItemButton.getScene().getWindow();
		stage.close();
	}
}
