//Tekijä: Vilma Mäkitalo
//Päivämäärä: 21.06.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), fragile(1-10, 1=weak)


package Controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Main.SmartPostManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.Window;

public class AddRoutePopupController implements Initializable{

	@FXML
	private ComboBox<String> fromPostCombo;
	@FXML
	private ComboBox<String> toPostCombo;
	@FXML
	private ComboBox<String> fromCityCombo;
	@FXML
	private ComboBox<String> toCityCombo;
	@FXML
	private Label labelText;
	@FXML
	private Button createRouteButton;
	
	SmartPostManager SPM;
	Connection con;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		SPM = SPM.getInstance();			//Create handler
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
		
		ArrayList<String> cities = SPM.getSmartPostCities(con);	//Set cities to comboboxes
		fromCityCombo.getItems().setAll(cities);
		toCityCombo.getItems().setAll(cities);

	}
	
	@FXML
	public void fromAction() {
		
		fromPostCombo.getItems().setAll(SPM.getOffices(fromCityCombo.getValue(), con));
	}
	
	@FXML
	public void toAction() {
		
		toPostCombo.getItems().setAll(SPM.getOffices(toCityCombo.getValue(), con));
	}
	
	@FXML
	public void createRouteAction(ActionEvent event){
		ArrayList<String> coor = SPM.getCoordinates(fromPostCombo.getValue(),toPostCombo.getValue(),con);
		
		if(coor.size() == 0) {
			labelText.setText("Valitse kaksi eri automaattia!");
		}
		else {
			
			SPM.createRoute(fromPostCombo.getValue(),toPostCombo.getValue(),con);
			SPM.setCoordinates(coor);
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			Stage stage = (Stage) createRouteButton.getScene().getWindow();
			stage.close();
		}
	}

}
