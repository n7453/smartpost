//Tekijä: Vilma Mäkitalo
//Päivämäärä: 28.06.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)

package Controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Main.SmartPostManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AddPackagePopupController implements Initializable{

	@FXML
	private Button saveToWarehouseButton;
	@FXML
	private Button sendPackageButton;
	@FXML
	private Button addNewItemButton;
	@FXML
	private Label textLabel;
	@FXML
	private Label errorTextLabel;
	@FXML
	private Label pgInfoLabel;
	@FXML
	private Label errorPackageLabel;
	@FXML
	private Label newWeightLabel;
	@FXML
	private Label newSizeLabel;
	@FXML
	private Label newFragileLabel;
	@FXML
	private Label newLenghtLabel;
	@FXML
	private Label classWeightLabel;
	@FXML
	private Label classSizeLabel;
	@FXML
	private Label classFragileLabel;
	@FXML
	private Label classLenghtLabel;
	@FXML
	private ComboBox<String> firstItemCombo;
	@FXML
	private ComboBox<String> secondItemCombo;
	@FXML
	private ComboBox<String> thirdItemCombo;
	@FXML
	private ComboBox<String> fourthItemCombo;
	@FXML
	private ComboBox<String> packageClassCombo;
	@FXML
	private ComboBox<String> chooseUserCombo;
	@FXML
	private TextField firstAmountField;
	@FXML
	private TextField secondAmountField;
	@FXML
	private TextField thirdAmountField;
	@FXML
	private TextField fourthAmountField;
	@FXML
	private TextField toNameField;
	
	SmartPostManager SPM;
	Connection con;
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		SPM = SPM.getInstance();	//Create handler
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
		
		ArrayList<String> items = SPM.getItems(con);	//Set items to comboboxes
		firstItemCombo.getItems().setAll(items);
		secondItemCombo.getItems().setAll(items);
		thirdItemCombo.getItems().setAll(items);
		fourthItemCombo.getItems().setAll(items);
		
		ArrayList<String> packageClass = SPM.getPackageClass(con);
		packageClassCombo.getItems().setAll(packageClass);
		
		newLenghtLabel.setText(SPM.getRouteLenght(SPM.getRouteID(),con) + "km");	//Gets routes length
		
		chooseUserCombo.getItems().setAll(SPM.getUsers(con));	//Gets all users
		
	}
	
	@FXML
	public void saveToWarehouseAction() {
		
		showIncludeInfoAction();
		
		String name = toNameField.getText();
		String user = chooseUserCombo.getValue();
		
		if(!name.equals("") && user != null) {	
			errorTextLabel.setText("Anna vain yksi saaja!");
			toNameField.setText("");
			chooseUserCombo.getSelectionModel().clearSelection();
		}
		else if(name.equals("") && user == null) {
			errorTextLabel.setText("Valitse tai anna saaja!");
		}
		else {
			//Make and test include/content
			int IncludeID = SPM.createInclude(firstItemCombo.getValue(),secondItemCombo.getValue(),thirdItemCombo.getValue(),fourthItemCombo.getValue(),firstAmountField.getText(),secondAmountField.getText(),thirdAmountField.getText(),fourthAmountField.getText(),con);	
			//Check if packageclass is ok
			String i = SPM.checkPackage(newWeightLabel.getText(),newSizeLabel.getText(),newFragileLabel.getText(),newLenghtLabel.getText(),classWeightLabel.getText(),classSizeLabel.getText(),classFragileLabel.getText(),classLenghtLabel.getText());
			if(IncludeID == 0) {
				errorTextLabel.setText("Valitse vähintään yksi \nesine ja syötä kaikille \nvalitsemillesi esineille \narvo kokonaislukuna!");
			}
			else if(i.contains("1") || i.contains("2") || i.contains("4") || i.contains("5")) {
				errorTextLabel.setText("Valitse sopiva \npostiluokka!");
				SPM.removeInclude(IncludeID,con);
			}
			else {
				if(name.equals("")) {
					SPM.saveToWarehouse(user,SPM.getRouteID(),packageClassCombo.getValue(),IncludeID, con);
				}
				else {
					SPM.saveToWarehouse(name,SPM.getRouteID(),packageClassCombo.getValue(),IncludeID, con);
				}
				
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				Stage stage = (Stage) saveToWarehouseButton.getScene().getWindow();
				stage.close();
			}
		}
	}
	
	@FXML
	public void sendPackageAction() {
		
		showIncludeInfoAction();
		
		String name = toNameField.getText();
		String user = chooseUserCombo.getValue();
		
		if(!name.equals("") && user != null) {
			errorTextLabel.setText("Anna vain yksi saaja!");
		}
		else if(name.equals("") && user == null) {
			errorTextLabel.setText("Valitse tai anna saaja!");
		}
		else {
			//Make and test include/content
			int IncludeID = SPM.createInclude(firstItemCombo.getValue(),secondItemCombo.getValue(),thirdItemCombo.getValue(),fourthItemCombo.getValue(),firstAmountField.getText(),secondAmountField.getText(),thirdAmountField.getText(),fourthAmountField.getText(),con);
			//Check if packageclass is ok
			String i = SPM.checkPackage(newWeightLabel.getText(),newSizeLabel.getText(),newFragileLabel.getText(),newLenghtLabel.getText(),classWeightLabel.getText(),classSizeLabel.getText(),classFragileLabel.getText(),classLenghtLabel.getText());
			if(IncludeID == 0) {
				errorTextLabel.setText("Valitse vähintään yksi \nesine ja syötä kaikille \nvalitsemillesi esineille \narvo kokonaislukuna!");
			}
			else if(i.contains("1") || i.contains("2") || i.contains("4") || i.contains("5")) {
				errorTextLabel.setText("Valitse sopiva \npostiluokka!");
				SPM.removeInclude(IncludeID,con);
			}
			else {
				if(name.equals("")) {
					SPM.saveToWarehouse(user,SPM.getRouteID(),packageClassCombo.getValue(),IncludeID, con);
				}
				else {
					SPM.saveToWarehouse(name,SPM.getRouteID(),packageClassCombo.getValue(),IncludeID, con);
				}
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				Stage stage = (Stage) sendPackageButton.getScene().getWindow();
				stage.close();
				SPM.setI(1);
			}
		}
	}
	
	@FXML
	public void showFirstAction() {
		
		String info = SPM.getItemInfo(firstItemCombo.getValue().toString(),con);
		textLabel.setText(info);		
	}
	
	@FXML
	public void showSecondAction() {
		
		String info = SPM.getItemInfo(secondItemCombo.getValue().toString(),con);
		textLabel.setText(info);
	}
	
	@FXML
	public void showThirdAction() {
		
		String info = SPM.getItemInfo(thirdItemCombo.getValue().toString(),con);
		textLabel.setText(info);
	}
	
	@FXML
	public void showFourthAction() {
		
		String info = SPM.getItemInfo(fourthItemCombo.getValue().toString(),con);
		textLabel.setText(info);
	}
	
	@FXML
	public void showClassInfoAction() {
		
		showIncludeInfoAction();
		textLabel.setText("");
		
		ArrayList<String> info = SPM.getPackageClassInfo(packageClassCombo.getValue(), con);	//Gets packageclass info
		classWeightLabel.setText(info.get(0));
		classSizeLabel.setText(info.get(1));
		classLenghtLabel.setText(info.get(2));
		classFragileLabel.setText(info.get(3));
		
		//Checks if package is ok
		String i = SPM.checkPackage(newWeightLabel.getText(),newSizeLabel.getText(),newFragileLabel.getText(),newLenghtLabel.getText(),classWeightLabel.getText(),classSizeLabel.getText(),classFragileLabel.getText(),classLenghtLabel.getText());
		pgInfoLabel.setText("");
		errorPackageLabel.setText("");
		if(i != "0") {
			if(i.contains("1")) {
				errorPackageLabel.setText("Pakettisi on liian \npainava tähän luokkaan!\n");
			}
			if(i.contains("2")) {
				errorPackageLabel.setText(errorPackageLabel.getText() + "Pakettisi on liian \niso tähän luokkaan!\n");
			}
			if(i.contains("3")) {
				errorPackageLabel.setText(errorPackageLabel.getText() + "Pakettiasi ei suositella \nlähetettäväksi tässä luokassa!\n");
			}
			if(i.contains("4")) {
				errorPackageLabel.setText(errorPackageLabel.getText() + "Reittisi on liian pitkä \ntähän pakettiluokkaan!\n");
			}
			if(i.contains("5")) {
				errorPackageLabel.setText(errorPackageLabel.getText() + "Valitse postiluokka!\n");
			}
			if(errorPackageLabel.getText() == "") {
				pgInfoLabel.setText("Postiluokka on sopiva!");
			}
		}
	}
	
	@FXML
	public void showIncludeInfoAction() {
		
		errorPackageLabel.setText("");
		pgInfoLabel.setText("");
		
		//Gets includes info
		ArrayList<String> print = SPM.getIncludeInfo(firstItemCombo.getValue(),secondItemCombo.getValue(),thirdItemCombo.getValue(),fourthItemCombo.getValue(),firstAmountField.getText(),secondAmountField.getText(),thirdAmountField.getText(),fourthAmountField.getText(),con);
		if(print.size() == 0) {
			errorPackageLabel.setText("Anna esineiden määrät \nnumeroina!");
		}
		else {
			newWeightLabel.setText(print.get(0));
			newSizeLabel.setText(print.get(1));
			newFragileLabel.setText(print.get(2));
			
			//Check if package is ok
			String i = SPM.checkPackage(newWeightLabel.getText(),newSizeLabel.getText(),newFragileLabel.getText(),newLenghtLabel.getText(),classWeightLabel.getText(),classSizeLabel.getText(),classFragileLabel.getText(),classLenghtLabel.getText());
			if(i != "0") {
				if(i.contains("1")) {
					errorPackageLabel.setText("Pakettisi on liian \npainava tähän luokkaan!\n");
				}
				if(i.contains("2")) {
					errorPackageLabel.setText(errorPackageLabel.getText() + "Pakettisi on liian \niso tähän luokkaan!\n");
				}
				if(i.contains("3")) {
					errorPackageLabel.setText(errorPackageLabel.getText() + "Pakettiasi ei suositella \nlähetettäväksi tässä luokassa!\n");
				}
				if(i.contains("4")) {
					errorPackageLabel.setText(errorPackageLabel.getText() + "Reittisi on liian pitkä \ntähän pakettiluokkaan!\n");
				}
				if(i.contains("5")) {
					errorPackageLabel.setText(errorPackageLabel.getText() + "Valitse postiluokka!\n");
				}
				if(errorPackageLabel.getText() == "") {
					pgInfoLabel.setText("Postiluokka on sopiva!");
				}
			}
		}
	}
	
	@FXML
	public void addNewItemAction() {
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Stage stage = (Stage) addNewItemButton.getScene().getWindow();
		stage.close();
		SPM.setI(2);
	}
	
	
}
