//Tekijä: Vilma Mäkitalo
//Päivämäärä: 10.07.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)

package Controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

import Main.SmartPostManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class LogInController implements Initializable{
	
	//Log in-tab
	@FXML
	private TextField userField;
	@FXML
	private PasswordField passwordField;
	@FXML
	private Button logInButton;
	@FXML
	private Button fShutDownButton;
	@FXML
	private Label logInLabel;
	
	//Sign in-tab
	@FXML
	private PasswordField newPasswordField;
	@FXML
	private PasswordField passwordDoubleField;
	@FXML
	private TextField safetyCodeField;
	@FXML
	private TextField newUserField;
	@FXML
	private TextField nameField;
	@FXML
	private RadioButton hostRadio;
	@FXML
	private RadioButton userRadio;
	@FXML
	private Button signInButton;
	@FXML
	private Label signInLabel;
	@FXML
	private Label errorLabel;
	
	
	SmartPostManager SPM;
	Connection con;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		SPM = SPM.getInstance();
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
	}
	
	@FXML
	public void logInAction() {
		
		int i = SPM.logIn(userField.getText(),passwordField.getText(),con);
		if(i == 0) {
			logInLabel.setText("Väärä käyttäjätunnus tai salasana!");
		}
		else {
		
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			Stage stage = (Stage) logInButton.getScene().getWindow();
			stage.close();
		}
	}
	
	@FXML
	public void signInAction() {
		
		errorLabel.setVisible(true);
		errorLabel.setText("");
		int i;
		
		//Check values
		if(!nameField.getText().equals("") && !newUserField.getText().equals("") && !newPasswordField.getText().equals("")) {
			if(hostRadio.isSelected()) {
				i = SPM.signInHost(nameField.getText(),newUserField.getText(),newPasswordField.getText(),passwordDoubleField.getText(),safetyCodeField.getText(),con);
				
				if(i == 0) {
					errorLabel.setText("Salasanat eivät vastaa toisiaan!");
				}
				else if(i == 1) {
					errorLabel.setText("Käyttäjätunnus tai salasana on liian pitkä!");
				}
				else if(i == 2) {
					errorLabel.setText("Luodaksesi ylläpitäjä tunnukset, tarvitset \nturvakoodin.");
				}
				else if(i == 3) {
					errorLabel.setText("Käyttäjätunnus on varattu!");
				}
				else {
					errorLabel.setVisible(false);
					signInLabel.setText("Rekisteröityminen onnistui. Voit nyt \nkirjautua sisään.");
				}
			}
			else if(userRadio.isSelected()) {
				i = SPM.signInUser(nameField.getText(),newUserField.getText(),newPasswordField.getText(),passwordDoubleField.getText(),con);
				
				if(i == 0) {
					errorLabel.setText("Salasanat eivät vastaa toisiaan!");
				}
				else if(i == 1) {
					errorLabel.setText("Käyttäjätunnus tai salasana on liian pitkä!");
				}
				else if(i == 2) {
					errorLabel.setText("Käyttäjätunnus on varattu!");
				}
				else {
					errorLabel.setVisible(false);
					signInLabel.setText("Rekisteröityminen onnistui. Voit nyt \nkirjautua sisään.");
				}		
			}
		}
		else {
			errorLabel.setText("Täytä kaikki kohdat!");
		}
	}
	
	@FXML
	public void hostRadioAction() {
		
		userRadio.setSelected(false);
	}
	
	@FXML
	public void userRadioAction() {
		hostRadio.setSelected(false);
	}

	@FXML
	public void shutDownAction() {
		
		SPM.setUser("shutDown");
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Stage stage = (Stage) logInButton.getScene().getWindow();
		stage.close();
	}
	
}

