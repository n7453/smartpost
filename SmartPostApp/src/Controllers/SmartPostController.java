//Tekijä: Vilma Mäkitalo
//Päivämäärä: 16.06.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)


package Controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Main.SmartPostManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class SmartPostController implements Initializable{

	@FXML
	private TabPane headTabPane;

	@FXML
	private Tab userTab;
	@FXML
	private TabPane userTabPane;
	@FXML
	private Tab userMainTab;

	@FXML
	private Tab hostTab;
	@FXML
	private TabPane hostTabPane;
	@FXML
	private Tab hostMainTab;
	
	@FXML
	private Button logOutButton;
	@FXML
	private Label companyNameLabel;
	@FXML
	private Label sloganLabel;
	
	//Host-tab
		//Main-tab
		@FXML
		private Button hostCreateItemButton;
		@FXML
		private Button removeItemButton;
		@FXML
		private Button addCodeButton;	
		@FXML
		private TextField newCodeField;
		@FXML
		private Label hostLabel;
		@FXML
		private Label redHostLabel;
	
		//Log-tab
		@FXML
		private ComboBox<String> chooseCombo;
		@FXML
		private Button showPackagesButton;
		@FXML
		private TableView<ObservableList<String>> packageTable;
		@FXML
		private ListView<String> itemListView;
		@FXML
		private Label sizeOfWarehouseLabel;
		@FXML
		private Label sizeOfLogLabel;
		
		//Calender-tab
		@FXML
		private DatePicker calender;
		@FXML
		private TableView<ObservableList<String>> warehouseTable;
		@FXML
		private TableView<ObservableList<String>> sentTable;
	
	
	//User-tab
		//Main-tab
		@FXML
		private Button userCreateItemButton;
		@FXML
		private Button showPostOnMapButton;
		@FXML
		private Button createPackageButton;
		@FXML
		private Button sendPackageButton;
		@FXML
		private Button removeRouteButton;
		@FXML
		private Label redUserLabel;
		@FXML
		private WebView mapWebView;
		@FXML
		private ComboBox<String> chooseCityCombo;
	
		//MyPackages-tab
		@FXML
		private ComboBox<String> myChooseCombo;
		@FXML
		private Button myShowPackagesButton;
		@FXML
		private TableView<ObservableList<String>> myPackageTable;
		@FXML
		private ListView<String> myItemList;
	
	//RemoveAccount-tab
		@FXML
		private Button removeWarningButton;
		@FXML
		private Label removeLabel;
		@FXML
		private Label redRemoveLabel;
		@FXML
		private Button removeAccountButton;
		
	SmartPostManager SPM;
	Connection con;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		SPM = SPM.getInstance();			//Create handle
		
		mapWebView.getEngine().load(getClass().getResource("/Main/index.html").toExternalForm());	//Set map to webview
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
		
		SPM.updateSafetyCode(con);		//Removes old safetycodes from database

		logInPopup();	//Popup for logging in or signing in 
		
		SPM.readXML("http://smartpost.ee/fi_apt.xml", con);	//Load/update SmartPost data
		
		ArrayList<String> cities = SPM.getSmartPostCities(con);	//Set cities which have SmartPosts to combobox
		chooseCityCombo.getItems().setAll(cities);
		
		chooseCombo.getItems().setAll("Varasto","Loki");	//Choices for log-tabs combobox
		myChooseCombo.getItems().setAll("Lähetetyt","Vastaanotetut");	//Choices for myPackage-tabs combobox
		
		setCalendarTables();	//Sets columns to calender tab
	}
		
	@FXML
	public void logOutAction() {

		newCodeField.setText("");	//Empties every table, field, view etc.
		hostLabel.setText("");
		redHostLabel.setText("");
		packageTable.getItems().clear();
		itemListView.getItems().clear();
		sizeOfWarehouseLabel.setText("");
		sizeOfLogLabel.setText("");
		warehouseTable.getItems().clear();
		sentTable.getItems().clear();
		redUserLabel.setText("");
		myPackageTable.getItems().clear();
		myItemList.getItems().clear();
		removeLabel.setText("");
		redRemoveLabel.setText("");
		removeRouteAction();
		
		
		int i  = SPM.getSecurityLevel(); //Gets users securitylevel. Hosts don't get check/bill
		
		if(i == 2) {
			SPM.printBill(con);
		}
		
		SPM.setUser(null);
		
		logOutButton.getScene().getWindow().hide();
	
		logInPopup();		//New log in popup so new user can log in or sign in
	}

	//Both Main-tabs
	@FXML
	public void showPostOnMapAction() {
		
		String input = chooseCityCombo.getValue();
		ArrayList<String> information = null;
		
		if (input == null) {
			redUserLabel.setText("Valitse automaatti!");
		}
		else {
			information = SPM.getDrawInformation(input, con);	//Get Smartposts' address and availability information in chosen city
		
			for(String s : information) {
				String[] S = s.split(":");
				String info = "document.goToLocation('" + S[0] + "', '" + S[1] + "', 'red')";
				mapWebView.getEngine().executeScript(info);
			}
		}
	}
	
	@FXML
	public void createPackageAction() {
		
		addRouteAction();
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(SPM.getRouteID() != 0) {
			SPM.createPackage();	//Create popup window for creating package
			
			con = SPM.getDataBase("SmartPost.db");	//Create connection to database
			
			int i = SPM.getI();	//Gets information which is set in AddPackagePopupController
			
			if(i == 1) {
				sendPackageAction();
			}
			else if(i == 2) {
				createItemAction();
			}
			
			updateLog();
		}
	}
	
	@FXML
	public void sendPackageAction() {
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		SPM.sendPackagePopup();	//Create popup window for sending package
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
		
		if(SPM.getI() == 1) { 	//Get information which is set in sendPackagePopupController
			createPackageAction();
		}
		else {
			String s = SPM.drawRoute(con);	//Get line for drawing road
			mapWebView.getEngine().executeScript(s);
			SPM.addToDrawnRoutes(s);	//Adds this route to drawn routes
		}
		
		updateLog();	
	}
	
	@FXML
	public void createItemAction() {

		newCodeField.setText("");
		hostLabel.setText("");
		redHostLabel.setText("");
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		SPM.createItemPopup();	//Create popup window for creating new item
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
		
		if(SPM.getI() == 1) {	//Gets information which is set in AddItemPopupController
			createPackageAction();
		}
	}
	
	public void addRouteAction() {
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		SPM.setRouteID(0);
		SPM.addRoutePopup();	//Create popup window for creating new route
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
		
		String s = "document.createPath("+ SPM.getCoordinates() + ", 'red', 1)"; //Make draw line
		Object lenght = mapWebView.getEngine().executeScript(s);	//Just get length for route, not draw
		removeRouteAction();
		
		ArrayList<String> drawnRoutes = SPM.getDrawnRoutes();	//Get routes which were drawn before removeRouteAction
		for(String str : drawnRoutes) {		//Draw them again
			mapWebView.getEngine().executeScript(str);
		}
		
		SPM.saveRoute(Double.parseDouble(lenght.toString()), con);	//Save route's length
	}
	
	@FXML
	public void removeRouteAction() {
		
		mapWebView.getEngine().executeScript("document.deletePaths()");	//Delete routes from map
		SPM.emptyDrawnRoutes();
	}

	public void addCodeAction() {
		
		hostLabel.setText("");
		redHostLabel.setText("");
		
		int i = SPM.addSafetyCode(newCodeField.getText(), con);	//Creates new safety code if it's ok
		if(i == 1) {
			redHostLabel.setText("Koodi ei saa sisältää erikoismerkkejä!");
		}
		else if(i == 2) {
			redHostLabel.setText("Koodi on jo olemassa!");
		}
		else {
			hostLabel.setText("Koodin lisäys onnistui!");
		}
	}
	
	public void removeItemAction() {
		
		newCodeField.setText("");
		hostLabel.setText("");
		redHostLabel.setText("");
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		SPM.removeItemPopup();	//Create window for removing item
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
	}
	
	//Log-tab
	@FXML
	public void showPackagesAction() {

		newCodeField.setText("");
		hostLabel.setText("");
		redHostLabel.setText("");
		itemListView.getItems().clear();
		packageTable.getColumns().clear();
		
		updateLog();
		
		String choice = chooseCombo.getValue();
		
		ArrayList<String> columns = new ArrayList<String>();	//Gather all columns we need for chosen table
		columns.add("PackageID");
		columns.add("From");
		columns.add("To");
		
		if(choice.equals("Varasto")) {
			columns.add("CreateDate");
			SPM.setBaseTable(columns,packageTable);	//Set columns for table
			packageTable.getItems().setAll(FXCollections.observableArrayList(SPM.getObPgFromWarehouse(con))); //Set rows in table
		}
		else {
			columns.add("SentDate");
			columns.add("Fractured");
			columns.add("Successful");
			SPM.setBaseTable(columns,packageTable);	//Set columns for table
			packageTable.getItems().setAll(FXCollections.observableArrayList(SPM.getLogInformation(con)));	//Set rows in table
		}
	}
	
	@FXML
	public void showItemsAction() {

		newCodeField.setText("");
		hostLabel.setText("");
		redHostLabel.setText("");
		
		itemListView.setItems(FXCollections.observableArrayList(SPM.getPackageInformation(packageTable.getSelectionModel().getSelectedItem().toString(),con)));	//Shows chosen package's information
	}
	
	public void updateLog() {
		
		sizeOfWarehouseLabel.setText(SPM.getWarehouseSize(con));	//Shows how many packages there are in warehouse and log
		sizeOfLogLabel.setText(SPM.getLogSize(con));
	}

	//Calender-tab
	public void setCalendarTables() {
		
		newCodeField.setText("");
		hostLabel.setText("");
		redHostLabel.setText("");
		warehouseTable.getColumns().clear();
		sentTable.getColumns().clear();

		ArrayList<String> columnsWH = new ArrayList<String>();
		columnsWH.add("PackageID");
		columnsWH.add("From");
		columnsWH.add("To");
		
		ArrayList<String> columnsLog = new ArrayList<String>();
		columnsLog.add("PackageID");
		columnsLog.add("From");
		columnsLog.add("To");
		columnsLog.add("CreateDate");
		columnsLog.add("Fractured");
		columnsLog.add("Successful");
		
		SPM.setBaseTable(columnsWH, warehouseTable);
		SPM.setBaseTable(columnsLog,sentTable);
	}
	
	@FXML
	public void showCalenderInfoAction() {
		
		LocalDate date = calender.getValue();
		
		sentTable.getItems().setAll(FXCollections.observableArrayList(SPM.getLogDate(date,con)));	//Sets rows to tables
		warehouseTable.getItems().setAll(FXCollections.observableArrayList(SPM.getWarehouseDate(date,con)));
	}
	
	//myPackage-tab
	@FXML
	public void myShowPackagesAction() {
		
		String choice = myChooseCombo.getValue();
		myPackageTable.getColumns().clear();
		
		ArrayList<String> columns = new ArrayList<String>();	//Gather all columns we need for chosen table
		columns.add("PackageID");
		columns.add("From");
		columns.add("To");
		
		if(choice.equals("Lähetetyt")) {
			columns.add("SentDate");
			SPM.setBaseTable(columns, myPackageTable);	//Set columns for table
			myPackageTable.getItems().setAll(FXCollections.observableArrayList(SPM.getMySentInformation(con)));	//Set rows in table
		}
		else {
			columns.add("ReceiveDate");
			columns.add("Fractured");
			columns.add("Successful");
			SPM.setBaseTable(columns,myPackageTable);	//Set columns for table
			myPackageTable.getItems().setAll(FXCollections.observableArrayList(SPM.getMyReceivedInformation(con)));	//Set rows in table
		}
	}
	
	@FXML
	public void myShowItemsAction() {
		
		myItemList.setItems(FXCollections.observableArrayList(SPM.getPackageInformation(myPackageTable.getSelectionModel().getSelectedItem().toString(),con)));	//Shows chosen package's information
	}
	
	//Start and log in methods
	public void askWarehouseReset() {
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		SPM.askWarehouseReset();	//Create popup for asking if it is wanted to reset warehouse
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
	}
	
	public void setSecurityLevel() {
		
		int i = 0;
		while(i == 0) {
			if(SPM.getUserName() == null) {		//If not logged in/no username, open new popup for logging in or singing in
				logInPopup();
			}
			else {
				i = 1;
			}
		}
		
		if(SPM.getUserName().equals("shutDown")) {	//If shutdownbutton is clicked in loginpopup
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			Stage stage = (Stage) logOutButton.getScene().getWindow();
			stage.close();
		}
		else {
			try {
				Stage stage = (Stage) logOutButton.getScene().getWindow();
				stage.show();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			
			int j = SPM.getSecurityLevel();	//Gets users securitylevel and sets the GUI to look right
			
			if(j == 2) {;
				headTabPane.getSelectionModel().select(userTab);
				userTabPane.getSelectionModel().select(userMainTab);
				userTab.setDisable(false);
				hostTab.setDisable(true);
				
				SPM.resetPgCounts();	//Resets lists that keep track sent and stored packages in that session		
			}
			else if(j == 1) {
				headTabPane.getSelectionModel().select(hostTab);
				hostTabPane.getSelectionModel().select(hostMainTab);
				userTab.setDisable(true);
				hostTab.setDisable(false);
								
				askWarehouseReset();	//Popup for asking if reset warehouse or continue with old information and check if logged in
				
				updateLog();
			}
		}
	}
	
	public void logInPopup() {
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		SPM.logInPopup();	//Popup for loging in or signing in
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
		
		removeRouteAction();
		
		setSecurityLevel();		//Sets the setup for right user
	}
	
	//RemoveAccount-tab
	@FXML
	public void removeWarningAction() {
		
		removeLabel.setText("");
		redRemoveLabel.setText("");
		
		removeLabel.setText("Haluatko varmasti poistaa tilisi? \nTiliä ei voi palauttaa poistamisen jälkeen."); //Gives warning and reveals the true removal button
		removeAccountButton.setVisible(true);
		removeAccountButton.setDisable(false);
	}
	
	@FXML
	public void removeAccountAction() {
		
		removeLabel.setText("");
		redRemoveLabel.setText("");
		
		int i = SPM.removeAccount(con);	//Removes account if it's not the last host account
		
		if(i == 1) {
			redRemoveLabel.setText("Et voi poistaa viimeistä ylläpito-tiliä!");
		}
		else {
			removeLabel.setText("");
			logOutAction();
		}
		removeAccountButton.setVisible(false);	//Hides the actual removal button
		removeAccountButton.setDisable(true);
	}
	
	
	
	
}