//Tekijä: Vilma Mäkitalo
//Päivämäärä: 28.06.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)

package Controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Main.SmartPostManager;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Callback;

public class SendPackagePopupController implements Initializable{

	@FXML
	private TableView<ObservableList<String>> packageTableView;
	@FXML
	private Button sendPackageButton;
	@FXML
	private Button newPackageButton;
	
	SmartPostManager SPM;
	Connection con;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		SPM = SPM.getInstance();	//Create handler
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
		
		SPM.setBaseTable(getColumns(),packageTableView);	//Set columns for table
		
		packageTableView.getItems().setAll(SPM.getUserPgFromWarehouse(SPM.getUserName(),con));	//Get users packages' infos
	}

	@FXML
	public void sendPackageAction() {
		
		SPM.sendPackage(packageTableView.getSelectionModel().getSelectedItem().toString(), con);
		
		try {
			
			Stage view = new Stage();		//Popup window for send message
			Parent page = FXMLLoader.load(getClass().getResource("/FXML/SendMessagePopup.fxml"));
			Scene scene = new Scene(page);
			
			view.setScene(scene);
			scene.getStylesheets().add(getClass().getResource("/Css/SendMessage.css").toExternalForm());
			view.show();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			con.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		Stage stage = (Stage) sendPackageButton.getScene().getWindow();
		stage.close();
		
		
	}
	
	@FXML
	public void newPackageAction() {
		
		SPM.setI(1);
		Stage stage = (Stage) newPackageButton.getScene().getWindow();
		stage.close();
	}
	
	public ArrayList<String> getColumns() {
		
		ArrayList<String> columns = new ArrayList<String>();
		columns.add("PackageID");
		columns.add("From");
		columns.add("To");
		columns.add("CreateDate");
		
		return columns;
	}
}
