//Tekijä: Vilma Mäkitalo
//Päivämäärä: 10.07.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)

package Controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

import Main.SmartPostManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

public class ResetWarehouseController implements Initializable{

	@FXML
	private CheckBox yesCheckBox;
	@FXML
	private CheckBox noCheckBox;
	@FXML
	private Button chooseButton;
	
	SmartPostManager SPM;
	Connection con;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		SPM = SPM.getInstance();	//Create handler
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
		
		noCheckBox.setSelected(true);
	}

	@FXML
	public void NoChooseAction() {
		
		yesCheckBox.setSelected(false);

	}
	
	@FXML
	public void YesChooseAction() {
		
		noCheckBox.setSelected(false);

	}
	
	@FXML
	public void chooseAction() {
		
		if(yesCheckBox.isSelected() == true) {
			
			SPM.emptyWarehouse(con);
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			Stage stage = (Stage) chooseButton.getScene().getWindow();
			stage.close();
		}
		
		else if(noCheckBox.isSelected() == true) {
			
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			Stage stage = (Stage) chooseButton.getScene().getWindow();
			stage.close();
		}
	}
	
}
