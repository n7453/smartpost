//Tekijä: Vilma Mäkitalo
//Päivämäärä: 28.06.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)

package Controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Main.SmartPostManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AddItemPopupController implements Initializable{

	@FXML
	private TextField getNameField;
	@FXML
	private ComboBox<Integer> getFragileCombo;
	@FXML
	private TextField getWeightField;
	@FXML
	private TextField getSizeField;
	@FXML
	private Button createItemButton; 
	@FXML
	private Button infoButton;
	@FXML
	private Button createPackageButton;
	@FXML
	private Label textLabel;
	@FXML
	private Label errorLabel;
	
	
	SmartPostManager SPM;
	Connection con;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		SPM = SPM.getInstance();	//Create handler
		
		getFragileCombo.getItems().addAll(1,2,3,4,5,6,7,8,9,10);
		
		con = SPM.getDataBase("SmartPost.db");	//Create connection to database
		
		setSecurityLevel();
		
	}
	
	@FXML
	public <E> void createItemAction() {
		
		textLabel.setText("");
		errorLabel.setText("");
		
		ArrayList<E> input = SPM.checkValues(getNameField.getText(), getFragileCombo.getValue(), getSizeField.getText(),getWeightField.getText());	//Checks if values are in correct forms
		if(input.isEmpty()) {
			errorLabel.setText("Täytä kaikki kentät! \nKoko ja paino \nnumeroina!");
		}
		else {
			int i = SPM.createItem(input,con);
			if(i == 0) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				Stage stage = (Stage) createItemButton.getScene().getWindow();
				stage.close();
			}
			else{
				errorLabel.setText("Esine on jo olemassa!");
			}
		}
	}
	
	@FXML
	public void createPackageAction() {
		
		createItemAction();
		if(errorLabel.getText().equals("")) {
			SPM.setI(1);	//Sents information to another controller
		}
	}
	
	@FXML
	public void infoAction() {
		
		textLabel.setText("");
		errorLabel.setText("");
		
		textLabel.setText("1 = heikko\n10 = vahva");
	}
	
	public void setSecurityLevel() {
		
		int i = SPM.getSecurityLevel();	//Get securitylevel and allows or disallows creating packages
		
		if(i == 1) {
			createPackageButton.setDisable(true);
			createPackageButton.setVisible(false);
		}
		else if(i == 2) {
			createPackageButton.setDisable(false);
			createPackageButton.setVisible(true);
		}
	}
}
