//Tekijä: Vilma Mäkitalo
//Päivämäärä: 28.06.2017
//Yksiköt: weight(g), size(dm³ = l), distance(km), speed(km/h), fragile(1-10, 1=weak)

package Controllers;

import java.net.URL;
import java.util.ResourceBundle;

import Main.SmartPostManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

public class SendMessagePopupController implements Initializable{

	@FXML
	private Label messageLabel;
	
	SmartPostManager SPM;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		SPM = SPM.getInstance();
		
		String message = SPM.getSendMessage();
		messageLabel.setText(message);
		
	}

}
